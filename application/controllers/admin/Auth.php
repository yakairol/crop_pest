<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
       $this->load->model('admin/auth_model', 'auth_model');
    }

    public function index(){
        if($this->session->has_userdata('is_admin_login'))
        {
            redirect('admin/dashboard');
        }
        else{
            redirect('admin/auth/login');
        }
    }

    public function login(){
        if($this->input->post('submit')){
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/auth/login');
            }
            else {
                $data = array(
                    'email' => $this->input->post('email'),
                    'password' => $this->input->post('password')
                );
                //print_r($data);
               // echo "<br>";
                $result = $this->auth_model->login($data);

                //$result1 = $this->auth_model->get_setting();
                if ($result == TRUE) {
                    $admin_data = array(
                        'admin_id' => $result['id'],
                        'name' => $result['username'],
                        'first_name' => $result['firstname'],
                        'role' => $result['is_admin'],
                        'email' => $result['email'],

                        'add_crop_pest' => $result['add_crop_pest'],
                        'view_crop_pest' => $result['view_crop_pest'],

                        'view_users' => $result['view_users'],
                        'add_users' => $result['add_users'],
                        'add_roles' => $result['add_roles'],
                        'add_permission' => $result['add_permission'],

                        'is_admin_login' => TRUE,
                        //'regdid' => '',
                        //'logo' => $result1['company_logo'],
                        //'com_name'  => $result1['company_name'],
                    );
                    $this->session->set_userdata($admin_data);
                    //print_r($admin_data);
                    if($result['is_admin'] == 'admin') {
                        redirect(base_url('admin/Dashboard'), 'refresh');
                    }
                    else{
                        redirect(base_url('admin/Dashboard'), 'refresh');
                    }

                }
            }
        }
        else{
            $this->load->view('admin/auth/login');
        }
    }


    public function change_pwd(){
        $id = $this->input->post('idd');
        if($this->input->post('submit')){
            $pass = $this->input->post('password');
            $cpass = $this->input->post('cpassword');
            if($pass != $cpass){
                $this->session->set_flashdata('msg', 'Password does not Match .!!');
                redirect(base_url('admin/Users/updateUserPassword'.'/'.$id));
            }else{
                $data = array(
                    'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT)
                );
                $result = $this->auth_model->change_pwd($data, $id);
                if($result){
                    $this->session->set_flashdata('msg', 'Password has been changed successfully!');
                    redirect(base_url('admin/Users'));
                }
            }
        }
        else{
            $data['view'] = 'admin/Users';
            $this->load->view('layout', $data);
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin/auth/login'), 'refresh');
    }

}  // end class


?>