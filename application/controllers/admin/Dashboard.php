<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    public function __construct(){
        parent::__construct();
    }
    public function index(){
        if($this->session->has_userdata('is_admin_login'))
        {
            $data['view'] = 'admin/dashboard';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }

    }

}