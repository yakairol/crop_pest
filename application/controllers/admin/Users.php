<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->model('Department_model');

        $this->load->model('admin/user_model', 'user_model');
        $this->load->library('upload');
    }

    public function index()
    {
        if ($this->session->has_userdata('is_admin_login')) {
            //$data['all_users'] = $this->user_model->get_all_users();
            $data['view'] = 'admin/users/user_list';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }
    }

    function user_list(){
        if ($this->session->has_userdata('is_admin_login')) {
            $fetch_data = $this->user_model->get_all_users();
            $data = array();
            foreach ($fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->firstname;
                $sub_array[] = $row->lastname;
                $sub_array[] = $row->email;
                $sub_array[] = $row->mobile_no;
                $sub_array[] =    ' <td>
                                     <a  class="btn btn-xs btn-primary"> '. $row->is_admin .'
                                        </a>
                            </td>';
                $sub_array[] = ' <td class="text-right">
                                <div class="btn-group">
                                     <span data-toggle="tooltip" data-placement="top" data-original-title="View Patient Details">
                                        <a id="' . $row->id . '" href="Users/userProfile'.'/'.$row->id.'" class="btn btn-icon-toggle"  data-toggle="modal" title="View User Profile">
                                        <i class="fa fa-user"></i>
                                        </a>
                                    </span>
                                    <span data-toggle="tooltip" data-placement="top" data-original-title="Edit Patient Profile">
                                        <a  id="' . $row->id . '" class="btn btn-icon-toggle" href="Users/updateUserProfile'.'/'.$row->id.'"   data-toggle="modal" title="Edit User Profile">
                                        <i class="fa fa-pencil"></i></a>
                                    </span>
                                      <span data-toggle="tooltip" data-placement="top" data-original-title="Change User Password">
                                        <a  id="' . $row->id . '"  href="Users/updateUserPassword'.'/'.$row->id.'" class="btn btn-icon-toggle"   data-toggle="modal"  title="Change User Password">
                                        <i class="fa fa-key"></i></a>
                                    </span>
                                        <span data-toggle="tooltip" data-placement="top" data-original-title="Delete User">
                                        <a  id="' . $row->id . '"  class="btn btn-icon-toggle deleteUser"   data-toggle="modal"  title="Delete User">
                                        <i class="fa fa-trash"></i></a>
                                    </span>
                                </div>
                                    
                                </td>';
                $data[] = $sub_array;
            }
            $output = array(
                "data" => $data
            );
            echo json_encode($output);
        }
        else{
            redirect('admin/auth/login');
        }
    }

    public function userProfile(){
        if($this->session->has_userdata('is_admin_login')){
            $id = $this->uri->segment(4);
            $data['user_details']=  $this->user_model->get_user_by_id($id);
            $data['view'] = 'admin/users/user_profile';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }

    }

    public function updateUserProfile(){
        if($this->session->has_userdata('is_admin_login')){
            $id = $this->uri->segment(4);
            $data['user_details']=  $this->user_model->get_user_by_id($id);
            $data['get_role'] =  $this->user_model->get_role_type();
            $data['view'] = 'admin/users/update_user_profile';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }

    }
    public function updateUserPassword(){
        if($this->input->post('submit')){
            redirect(base_url('admin/Users'));
        }
        else{
            if($this->session->has_userdata('is_admin_login')){
                $data['view'] = 'admin/users/update_user_password';
                $this->load->view('layout', $data);
            }
            else{
                redirect('admin/auth/login');
            }
        }
    }

    function delete_user()
    {
        $this->user_model->delete_user($_POST["user_id"]);
        //echo 'Record Deleted Successful';
       // redirect("Lab_cat", "refresh");
    }

    public function updateUserProfile2()
    {
        $previousPic =  $this->input->post('uploadLogo2');
        $user_id =  $this->input->post('idd');
        //echo $user_id ;
        //$doctor_email = $this->session->userdata('userId');
        $this->load->helper(array('form', 'url'));
        $config['upload_path'] = './uploads/images/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        //$this->load->library('upload', $config);
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('uploadLogo')) {
            $data = array(
                'firstname' => $this->input->post('firstName'),
                'lastname' => $this->input->post('LastName'),
                'username' => $this->input->post('userName'),
                'email' => $this->input->post('email'),
                'mobile_no' => $this->input->post('mobile_number'),
                'updated_at	'=>date('Y-m-d H:i:s'),
                'is_admin' =>   $this->input->post('role'),
            );
            $this->user_model->edit_pp($data,$user_id);
        } else {
            if($previousPic != ''){
                array_map('unlink', array_filter((array) array_merge(glob("./uploads/images/$previousPic"))));
            }
            $data2 = array(
                'firstname' => $this->input->post('firstName'),
                'lastname' => $this->input->post('LastName'),
                'username' => $this->input->post('userName'),
                'email' => $this->input->post('email'),
                'mobile_no' => $this->input->post('mobile_number'),
                'updated_at	'=>date('Y-m-d H:i:s'),
                'is_admin' =>   $this->input->post('role'),
                'profile_pic' => $this->upload->data('file_name'),
            );
            $this->user_model->edit_pp($data2,$user_id );
        }

    }

    public function add(){
        $this->load->library('upload');

        if($this->input->post('submit')){

            $this->form_validation->set_rules('firstname', 'Username', 'trim|required');
            $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('user_role', 'User Role', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['view'] = 'admin/users/user_add';
                $this->load->view('layout', $data);
            }
            else{
                if($_FILES['image']['name'] == ''){
                    $fileName = '';
                }else{
                    $fileName = time() . rand(11, 99) . basename($_FILES['image']['name']);
                }
                $fileTmp = $_FILES["image"]["tmp_name"];
                $fileType = $_FILES["image"]["type"];
                $fileSize = $_FILES["image"]["size"];
                $fileExt = substr($fileName, strrpos($fileName, ".") + 1);

                // Specify the images upload path
                $largeImageLoc = 'uploads/' . $fileName;
                $thumbImageLoc = 'uploads/images/' . $fileName;

                // Check and validate file extension
                if ((!empty($_FILES["image"])) && ($_FILES["image"]["error"] == 0)) {
                    if ($fileExt != "JPG" && $fileExt != "JPEG" && $fileExt != "PNG" && $fileExt != "GIF") {
                        $error = "Sorry, only JPG, JPEG, PNG, and GIF files are allowed.";
                    }
                } else {
                    $error = "Select an image file to upload.";
                }

                // If everything is ok, try to upload file
                if (empty($error) && !empty($fileName)) {

                    if (move_uploaded_file($fileTmp, $largeImageLoc)) {
                        // File permission
                        chmod($largeImageLoc, 0777);

                        // Get dimensions of the original image
                        list($width_org, $height_org) = getimagesize($largeImageLoc);

                        // Get image coordinates
                        $x = (int)$_POST['x'];
                        $y = (int)$_POST['y'];
                        $width = (int)$_POST['w'];
                        $height = (int)$_POST['h'];

                        // Define the size of the cropped image
                        $width_new = $width;
                        $height_new = $height;

                        // Create new true color image
                        $newImage = imagecreatetruecolor($width_new, $height_new);

                        // Create new image from file
                        switch ($fileType) {
                            case "image/gif":
                                $source = imagecreatefromgif($largeImageLoc);
                                break;
                            case "image/pjpeg":
                            case "image/jpeg":
                            case "image/jpg":
                                $source = imagecreatefromjpeg($largeImageLoc);
                                break;
                            case "image/png":
                            case "image/x-png":
                                $source = imagecreatefrompng($largeImageLoc);
                                break;
                        }

                        // Copy and resize part of the image
                        imagecopyresampled($newImage, $source, 0, 0, $x, $y, $width_new, $height_new, $width, $height);

                        // Output image to file
                        switch ($fileType) {
                            case "image/gif":
                                imagegif($newImage, $thumbImageLoc);
                                break;
                            case "image/pjpeg":
                            case "image/jpeg":
                            case "image/jpg":
                                imagejpeg($newImage, $thumbImageLoc, 90);
                                break;
                            case "image/png":
                            case "image/x-png":
                                imagepng($newImage, $thumbImageLoc);
                                break;
                        }

                        unlink($largeImageLoc);
                    } else {
                        $error = "Sorry, there was an error uploading your file.";
                    }
                }
                $data = array(
                    'username' => $this->input->post('firstname').' '.$this->input->post('lastname'),
                    'firstname' => $this->input->post('firstname'),
                    'lastname' => $this->input->post('lastname'),
                    'email' => $this->input->post('email'),
                    'mobile_no' => $this->input->post('mobile_no'),
                    'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                    'is_admin' => $this->input->post('user_role'),
                    'created_at' => date('Y-m-d : h:m:s'),
                    'updated_at' => date('Y-m-d : h:m:s'),
                    'profile_pic' => $fileName,
                );
                $data = $this->security->xss_clean($data);
                $result = $this->user_model->add_user($data);
                if($result){
                    $this->session->set_flashdata('msg', 'Record is Added Successfully!');
                    //redirect(base_url('admin/users'));
                    redirect("admin/users", "refresh");
                }
            }
        }
        else{
            if ($this->session->has_userdata('is_admin_login')) {
                //$data['deprt'] = $this->Department_model->list_department();

                $data['get_role'] =  $this->user_model->get_role_type();
                $data['view'] = 'admin/users/user_add';
                $this->load->view('layout', $data);
            }
            else{
                redirect('admin/auth/login');
            }

        }

    }

    public function role_list()
    {
        if ($this->session->has_userdata('is_admin_login')) {
            //$data['all_users'] = $this->user_model->get_all_users();
            $data['view'] = 'admin/users/role_list';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }
    }

    public function add_role_list(){
        if ($this->session->has_userdata('is_admin_login')) {
            $fetch_data =  $this->user_model->get_role_type();
            $data = array();
            $i = 1;
            foreach($fetch_data as $row)
            {
                $sub_array = array();
                $sub_array[] = $i;
                $sub_array[] = $row->role_type_name;
                $sub_array[] = ' <td class="text-right">
                                <div class="btn-group">
                                    <span data-toggle="tooltip" data-placement="top" data-original-title="Edit Role">
                                        <a class="btn btn-icon-toggle updateRole"  id="' . $row->role_id . '" data-toggle="modal" title="Edit Role"><i class="fa fa-pencil"></i></a>
                                    </span>
                                      <span data-toggle="tooltip" data-placement="top" data-original-title="Delete Patient ">
                                        <a class="btn btn-icon-toggle deleteRole"  id="' . $row->role_id . '" data-toggle="modal" title="Delete Role"><i class="fa fa-trash-o"></i></a>
                                    </span>
                                </div>
                                </td>';
                $data[] = $sub_array;
                $i++;
            }
            $output = array(
                "data"              => $data
            );
            echo json_encode($output);
        } else {
            redirect('admin/auth/login');
        }
    }

    public function add_role()
    {
        $_POST['action'] = "ADD";
        if ($_POST['action'] == "ADD") {
            $add_data = array(
                'role_type_name' => $this->input->post('roleName'),
            );
            $this->user_model->add_role_type($add_data);
            //redirect("Add_Bedcategory", "refresh");
        }
    }


    public function fetchRole()
    {
        $output = array();
        $data = $this->user_model->fetchRole($_POST["user_id"]);
        foreach ($data as $row)
        {
            $output['roleName'] = $row->role_type_name;
        }
        echo json_encode($output);
    }

    public function updateRole()
    {
        $_POST['action'] = "save";
        if ($_POST['action'] == "save") {
            $updated_data = array(
                'role_type_name' => $this->input->post('editRoleName'),
            );
            $this->user_model->updateRole($this->input->post("user_id"), $updated_data);
        }
    }
    public function deleteRole(){
        $this->user_model->deleteRole($_POST["role_id"]);
    }
    public function set_permission(){
        if($this->input->post('submit')){
            $result = '';
            $data1 = $this->input->post('role');
            //$bed_category = $this->input->post('bed_category');
            $len = count($data1);
            //print_r($data1);
            //echo '<br>';
            //print_r($bed_category);
            echo '<br>';
            for($i =0; $i<$len; $i++){
                echo "<br>";
                $add_crop_pest= $this->input->post('add_crop_pest');
                $view_crop_pest = $this->input->post('view_crop_pest');

                $view_users = $this->input->post('view_users');
                $add_users= $this->input->post('add_users');
                $add_roles= $this->input->post('add_roles');
                $add_permission = $this->input->post('add_permission');

                $data = array(
                    'add_crop_pest' =>isset($add_crop_pest[$i]) ? $add_crop_pest[$i] : 0,
                    'view_crop_pest' =>isset($view_crop_pest[$i]) ? $view_crop_pest[$i] : 0,

                    'view_users' =>isset($view_users[$i]) ? $view_users[$i] : 0,
                    'add_users' =>isset($add_users[$i]) ? $add_users[$i] : 0,
                    'add_roles' =>isset($add_roles[$i]) ? $add_roles[$i] : 0,
                    'add_permission' =>isset($add_permission[$i]) ? $add_permission[$i] : 0,
                );
                //$result = $this->user_model->get_role_type($data1[$i],$data);
                $result = $this->user_model->set_role_type_permission($data1[$i],$data);
            }

            if($result){
                $this->session->set_flashdata('msg', 'Record is update Successfully!');
               redirect("admin/users/set_permission");
            }

        }
        else{
            if ($this->session->has_userdata('is_admin_login')) {
                //$data['get_page'] =  $this->user_model->get_page_link();
                $data['get_user_role'] =  $this->user_model->get_role_type();
                $data['view'] = 'admin/users/permission2';
                $this->load->view('layout', $data);
            }
            else{
                redirect('admin/auth/login');
            }

        }
    }

}