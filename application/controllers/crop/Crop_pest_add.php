<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crop_pest_add extends MY_Controller {
    public function __construct(){
        parent::__construct();

        $this->load->library('session');
        //$this->load->model('admin/user_model', 'user_model');
        $this->load->model('crop/crop_pest_model_add');


    }
    public function index(){
        if($this->session->has_userdata('is_admin_login'))
        {
            $data['view'] = 'crop/crop_pest';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }
    }
    public function submit_form_one(){

        $data1 = array(
            'pest_det_category' => $this->input->post('pest_det_category'),
            'pest_det_crop ' => $this->input->post('pest_det_crop'),
            'pest_det_pest' => $this->input->post('pest_det_pest'),
            'pest_det_uni_id' => $this->input->post('uni_id'),
            'pest_det_uploader' => $this->session->userdata('admin_id'),
        );
        $data11 = $this->security->xss_clean($data1);
        $this->crop_pest_model_add->pest_det($data11);
        $this->session->set_flashdata('msg', 'Record is Added Successfully!');


       $data2 = array(
            'paper_reference_category' => $this->input->post('paper_reference_category'),
            'paper_reference_crop ' => $this->input->post('paper_reference_crop'),
            'paper_reference_pest' => $this->input->post('paper_reference_pest'),

            'paper_reference_pest_status' => $this->input->post('paper_reference_pest_status'),

            'paper_reference_uni_id' => $this->input->post('uni_id'),
            'paper_reference_uploader' => $this->session->userdata('admin_id'),
        );
        $data22 = $this->security->xss_clean($data2);
        $this->crop_pest_model_add->paper_reference($data22);

        $syn_one = $this->input->post('taxonomy_syn_name_one');
        $syn_two = $this->input->post('taxonomy_syn_name_two');
        $syn_three= $this->input->post('taxonomy_syn_name_three');

        if(!empty($syn_one)) {
            $data_one = array(
                'taxonomy_syn_name' => $this->input->post('taxonomy_syn_name_one'),
                'taxonomy_syn_ref ' => $this->input->post('taxonomy_syn_ref_one'),
                'taxonomy_syn_year' => $this->input->post('taxonomy_syn_year_one'),

                'taxonomy_syn_uni_id' => $this->input->post('uni_id'),
                'taxonomy_syn_uploader' => $this->session->userdata('admin_id'),
            );
            $data_1 = $this->security->xss_clean($data_one);
            $this->crop_pest_model_add->taxonomy_syn($data_1);
        }
        if(!empty($syn_two)) {
            $data_two = array(
                'taxonomy_syn_name' => $this->input->post('taxonomy_syn_name_two'),
                'taxonomy_syn_ref ' => $this->input->post('taxonomy_syn_ref_two'),
                'taxonomy_syn_year' => $this->input->post('taxonomy_syn_year_two'),

                'taxonomy_syn_uni_id' => $this->input->post('uni_id'),
                'taxonomy_syn_uploader' => $this->session->userdata('admin_id'),
            );
            $data_2 = $this->security->xss_clean($data_two);
            $this->crop_pest_model_add->taxonomy_syn($data_2);
        }
        if(!empty($syn_three)) {
            $data_three = array(
                'taxonomy_syn_name' => $this->input->post('taxonomy_syn_name_three'),
                'taxonomy_syn_ref ' => $this->input->post('taxonomy_syn_ref_three'),
                'taxonomy_syn_year' => $this->input->post('taxonomy_syn_year_three'),
                'taxonomy_syn_uni_id' => $this->input->post('uni_id'),
                'taxonomy_syn_uploader' => $this->session->userdata('admin_id'),
            );
            $data_3 = $this->security->xss_clean($data_three);
            $this->crop_pest_model_add->taxonomy_syn($data_3);
        }


        $data3 = array(
            'taxonomy_kingdom' => $this->input->post('taxonomy_kingdom'),
            'taxonomy_phylum ' => $this->input->post('taxonomy_phylum'),
            'taxonomy_class' => $this->input->post('taxonomy_class'),
            'taxonomy_order' => $this->input->post('taxonomy_order'),
            'taxonomy_family ' => $this->input->post('taxonomy_family'),
            'taxonomy_genus' => $this->input->post('taxonomy_genus'),
            'taxonomy_species' => $this->input->post('taxonomy_species'),
            'taxonomy_sub_species ' => $this->input->post('taxonomy_sub_species'),
            'taxonomy_variety' => $this->input->post('taxonomy_variety'),
            'taxonomy_scientific_name' => $this->input->post('taxonomy_scientific_name'),
            'taxonomy_accepted_name ' => $this->input->post('taxonomy_accepted_name'),
            'taxonomy_reference' => $this->input->post('taxonomy_reference'),
            'taxonomy_published_year' => $this->input->post('taxonomy_published_year'),
            'taxonomy_published_in ' => $this->input->post('taxonomy_published_in'),

            'taxonomy_nomen_code' => $this->input->post('taxonomy_nomen_code'),
            'taxonomy_published' => $this->input->post('taxonomy_published'),

            'taxonomy_uni_id' => $this->input->post('uni_id'),
            'taxonomy_uploader' => $this->session->userdata('admin_id'),
        );
        $data33 = $this->security->xss_clean($data3);
        $this->crop_pest_model_add->taxonomy($data33);

        $data4 = array(
            'taxonomy_common_name' => $this->input->post('taxonomy_common_name'),
            'taxonomy_vernacular_name ' => $this->input->post('taxonomy_vernacular_name'),

            'taxonomy_name_uni_id' => $this->input->post('uni_id'),
            'taxonomy_name_uploader' => $this->session->userdata('admin_id'),
        );
        $data44 = $this->security->xss_clean($data4);
        $this->crop_pest_model_add->taxonomy_name($data44);


        $data5 = array(
            'natural_history_intro' => $this->input->post('natural_history_intro'),
            'natural_history_intro_ref ' => $this->input->post('natural_history_intro_ref'),
            'natural_history_stage' => $this->input->post('natural_history_stage'),
            'natural_history_stage_ref' => $this->input->post('natural_history_stage_ref'),
            'natural_history_affected_part ' => $this->input->post('natural_history_affected_part'),

            'natural_history_affected_part_ref' => $this->input->post('natural_history_affected_part_ref'),
            'natural_history_damage_symtoms' => $this->input->post('natural_history_damage_symtoms'),
            'natural_history_position_site' => $this->input->post('natural_history_position_site'),
            'natural_history_position_site_ref' => $this->input->post('natural_history_position_site_ref'),
            'natural_history_pupation_site' => $this->input->post('natural_history_pupation_site'),
            'natural_history_pupation_site_ref' => $this->input->post('natural_history_pupation_site_ref'),
            'natural_history_diapause' => $this->input->post('natural_history_diapause'),
            'natural_history_diapause_ref' => $this->input->post('natural_history_diapause_ref'),
            'natural_history_uni_id' => $this->input->post('uni_id'),
            'natural_history_uploader' => $this->session->userdata('admin_id'),
        );
        $data55 = $this->security->xss_clean($data5);
        $this->crop_pest_model_add->natural_history($data55);

        $data6 = array(
            'distribution_world_api' => $this->input->post('distribution_world_api'),
            'distribution_india_api ' => $this->input->post('distribution_india_api'),
            'distribution_district ' => $this->input->post('distribution_district'),
            'distribution_worldlat ' => $this->input->post('world_dis_lat'),
            'distribution_worldlng ' => $this->input->post('world_dis_lng'),
            'distribution_worldcity ' => $this->input->post('world_dis_city'),
            'distribution_worldstate ' => $this->input->post('world_dis_state'),
            'distribution_worldcou ' => $this->input->post('world_dis_country'),
            'distribution_indialat ' => $this->input->post('ind_dis_lat'),
            'distribution_indialng ' => $this->input->post('ind_dis_lng'),
            'distribution_indiacity ' => $this->input->post('ind_dis_city'),
            'distribution_indiastate ' => $this->input->post('ind_dis_state'),
            'distribution_uni_id' => $this->input->post('uni_id'),
            'distribution_uploader' => $this->session->userdata('admin_id'),
        );

        $data66 = $this->security->xss_clean($data6);
        $this->crop_pest_model_add->distribution($data66);
        $this->session->set_flashdata('msg', 'Record is Added Successfully!');

        $response = 'success';
        echo json_encode($response);

    }

    public function submit_form_two(){

        $data1 = array(
            'life_cycle_field_condition' => $this->input->post('life_cycle_field_condition'),
            'life_cycle_field_condition_ref ' => $this->input->post('life_cycle_field_condition_ref'),
            'life_cycle_oviposition_site' => $this->input->post('life_cycle_oviposition_site'),

            'life_cycle_oviposition_site_ref' => $this->input->post('life_cycle_oviposition_site_ref'),
            'life_cycle_diapause ' => $this->input->post('life_cycle_diapause'),
            'life_cycle_diapause_ref' => $this->input->post('life_cycle_diapause_ref'),
            'life_cycle_pupation_site' => $this->input->post('life_cycle_pupation_site'),

            'life_cycle_pupation_site_ref ' => $this->input->post('life_cycle_pupation_site_ref'),
            'life_cycle_lab_condition' => $this->input->post('life_cycle_lab_condition'),
            'life_cycle_lab_condition_ref' => $this->input->post('life_cycle_lab_condition_ref'),

            'life_cycle_bio_ecology ' => $this->input->post('life_cycle_bio_ecology'),
            'life_cycle_bio_ecology_ref' => $this->input->post('life_cycle_bio_ecology_ref'),
            'life_cycle_dispersal' => $this->input->post('life_cycle_dispersal'),
            'life_cycle_dispersal_ref ' => $this->input->post('life_cycle_dispersal_ref'),
            'life_cycle_oviposition_site' => $this->input->post('life_cycle_oviposition_site'),
            'life_cycle_uni_id' => $this->input->post('uni_id'),
            'life_cycle_uploader' => $this->session->userdata('admin_id'),
        );
        $data11 = $this->security->xss_clean($data1);
        $this->crop_pest_model_add->life_cycle($data11);
        $this->session->set_flashdata('msg', 'Record is Added Successfully!');


        $data2 = array(
            'prevention_tel' => $this->input->post('prevention_tel'),
            'prevention_etl_ref ' => $this->input->post('prevention_etl_ref'),
            'prevention_cultural' => $this->input->post('prevention_cultural'),
            'prevention_cultural_ref' => $this->input->post('prevention_cultural_ref'),

            'prevention_mechanical' => $this->input->post('prevention_mechanical'),

            'prevention_mechanical_ref' => $this->input->post('prevention_mechanical_ref'),
            'prevention_physical' => $this->input->post('prevention_physical'),
            'prevention_physical_ref' => $this->input->post('prevention_physical_ref'),

            'prevention_biological' => $this->input->post('prevention_biological'),
            'prevention_biological_ref' => $this->input->post('prevention_biological_ref'),
            'prevention_insecticide' => $this->input->post('prevention_insecticide'),
            'prevention_insecticide_ref' => $this->input->post('prevention_insecticide_ref'),

            'prevention_trade_name' => $this->input->post('prevention_trade_name'),
            'prevention_company' => $this->input->post('prevention_company'),
            'prevention_active_ingredient' => $this->input->post('prevention_active_ingredient'),
            'prevention_dosage' => $this->input->post('prevention_dosage'),
            'prevention_resistant_variety' => $this->input->post('prevention_resistant_variety'),
            'prevention_resistant_variety_ref' => $this->input->post('prevention_resistant_variety_ref'),
            'prevention_tolerant_name' => $this->input->post('prevention_tolerant_name'),
            'prevention_tolerant_name_ref' => $this->input->post('prevention_tolerant_name_ref'),
            'prevention_uni_id' => $this->input->post('uni_id'),
            'prevention_uploader' => $this->session->userdata('admin_id'),
        );
        $data22 = $this->security->xss_clean($data2);
        $this->crop_pest_model_add->prevention($data22);

        $response= 'success';
        echo json_encode($response);

    }
    public function submit_form_three(){
        $data2 = array(
            'parasite_name' => $this->input->post('parasite_name'),
            'parasite_stage ' => $this->input->post('parasite_stage'),
            'parasite_ref' => $this->input->post('parasite_ref'),

            'parasite_uni_id' => $this->input->post('uni_id'),
            'parasite_uploader' => $this->session->userdata('admin_id'),
        );
        $data22 = $this->security->xss_clean($data2);
        $this->crop_pest_model_add->parasite($data22);

        $data3 = array(
            'pradators_name' => $this->input->post('pradators_name'),
            'pradators_stage ' => $this->input->post('pradators_stage'),
            'pradators_ref' => $this->input->post('pradators_ref'),

            'pradators_uni_id' => $this->input->post('uni_id'),
            'pradators_uploader' => $this->session->userdata('admin_id'),
        );
        $data33 = $this->security->xss_clean($data3);
        $this->crop_pest_model_add->pradators($data33);

        $data = array(
            'yield_loss_etl' => $this->input->post('yield_loss_etl'),
            'yield_loss_name ' => $this->input->post('yield_loss_name'),
            'yield_loss_ref' => $this->input->post('yield_loss_ref'),

            'yield_loss_uni_id' => $this->input->post('uni_id'),
            'yield_loss_uploader' => $this->session->userdata('admin_id'),
        );
        $datas = $this->security->xss_clean($data);
        $this->crop_pest_model_add->yield_loss($datas);
        $data7 = array(
            'add_info_name' => $this->input->post('add_info_name'),
            'add_info_ref ' => $this->input->post('add_info_ref'),

            'add_info_uni_id' => $this->input->post('uni_id'),
            'add_info_uploader' => $this->session->userdata('admin_id'),
        );
        $data77 = $this->security->xss_clean($data7);
        $this->crop_pest_model_add->add_info($data77);


        $this->session->set_flashdata('msg', 'Record is Added Successfully!');
        $response['redirect'] = base_url('crop/crop_pest_view');
        echo json_encode($response);

    }



}