<?php
/**
 * Created by PhpStorm.
 * User: Yakairol-Charan
 * Date: 10-12-2020
 * Time: 12:23
 */

class Crop_pest_image extends  CI_Controller
{
    public function __construct(){
        parent::__construct();

        $this->load->library('session');
        //$this->load->model('admin/user_model', 'user_model');
        $this->load->model('crop/crop_pest_model_add');
    }
    public function general_image(){
        if(!empty($_FILES)){
            // File upload configuration
            $uploadPath = 'uploads/test';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = '*';

            // Load and initialize upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            // Upload file to the server
            if($this->upload->do_upload('file')){
                $fileData = $this->upload->data();
                $uploadData['general_images_name'] = $fileData['file_name'];
//                $uploadData['uploaded_on'] = date("Y-m-d H:i:s");
                $uploadData['general_images_uni_id'] =$this->input->post('uni_id');
                $uploadData['general_images_uploader'] =$this->session->userdata('admin_id');

                // Insert files info into the database
                $insert = $this->crop_pest_model_add->general_image($uploadData);
            }
        }
    }

    public function symtom_image(){
        if(!empty($_FILES)){
            // File upload configuration
            $uploadPath = 'uploads/test';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = '*';

            // Load and initialize upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            // Upload file to the server
            if($this->upload->do_upload('file')){
                $fileData = $this->upload->data();
                $uploadData['symtom_images_name'] = $fileData['file_name'];
                //$uploadData['uploaded_on'] = date("Y-m-d H:i:s");
                $uploadData['symtom_images_uploader'] =$this->session->userdata('admin_id');
                $uploadData['symtom_images_uni_id'] =$this->input->post('uni_id');

                // Insert files info into the database
                $insert = $this->crop_pest_model_add->symtom_image($uploadData);
            }
        }
    }

    public function lifecycle_image(){
        if(!empty($_FILES)){
            // File upload configuration
            $uploadPath = 'uploads/test';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = '*';

            // Load and initialize upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            // Upload file to the server
            if($this->upload->do_upload('file')){
                $fileData = $this->upload->data();
                $uploadData['ife_cycle_images_name'] = $fileData['file_name'];

                //$uploadData['uploaded_on'] = date("Y-m-d H:i:s");
                $uploadData['ife_cycle_images_uploader'] =$this->session->userdata('admin_id');
                $uploadData['ife_cycle_images_uni_id'] =$this->input->post('uni_id');
                $uploadData['ife_cycle_images_ref'] =$this->input->post('lifecycle_imgref');

                // Insert files info into the database
                $insert = $this->crop_pest_model_add->lifecycle_image($uploadData);
            }
        }
    }



}