<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crop_pest_view extends MY_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Crop_pest_model');
        //$this->load->model('admin/Setting_model', 'Setting_model');
    }
    public function index(){
        if($this->session->has_userdata('is_admin_login'))
        {
            $data['view'] = 'crop/crop_pest_view';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }
    }

    public function cropDetails(){
        if($this->session->has_userdata('is_admin_login')){
           // $id = $this->uri->segment(4);
            $uni_id = 1;
            $uploader_id = 2;
            //$uni_id = '5fd1fa16c5de5';
            //$uploader_id = 1;
            $data['taxo']=  $this->Crop_pest_model->cropDetails($uni_id,$uploader_id);
            $data['taxo2']=  $this->Crop_pest_model->cropDetails2($uni_id,$uploader_id);
            $data['taxo3']=  $this->Crop_pest_model->cropDetails3($uni_id,$uploader_id);
            $data['taxo4']=  $this->Crop_pest_model->cropDetails4($uni_id,$uploader_id);
            $data['taxo5']=  $this->Crop_pest_model->cropDetails5($uni_id,$uploader_id);
            $data['taxo6']=  $this->Crop_pest_model->cropDetails6($uni_id,$uploader_id);
            $data['taxo7']=  $this->Crop_pest_model->cropDetails7($uni_id,$uploader_id);
            $data['taxo8']=  $this->Crop_pest_model->cropDetails8($uni_id,$uploader_id);
            $data['taxo9']=  $this->Crop_pest_model->cropDetails9($uni_id,$uploader_id);
            $data['taxo10']=  $this->Crop_pest_model->cropDetails10($uni_id,$uploader_id);
            $data['taxo11']=  $this->Crop_pest_model->cropDetails11($uni_id,$uploader_id);
            $data['taxo12']=  $this->Crop_pest_model->cropDetails12($uni_id,$uploader_id);
            $data['taxo13']=  $this->Crop_pest_model->cropDetails13($uni_id,$uploader_id);
            //print_r($this->Crop_pest_model->cropDetails(1));
            $data['view'] = 'crop/crop_details';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }

    }
    public function cropDetailsTesting(){
        if($this->session->has_userdata('is_admin_login')){
            // $id = $this->uri->segment(4);
            $uni_id = '5fd1fa16c5de5';
            $uploader_id = 1;
            $data['taxo']=  $this->Crop_pest_model->cropDetails($uni_id,$uploader_id);
            $data['taxo2']=  $this->Crop_pest_model->cropDetails2($uni_id,$uploader_id);
            $data['taxo3']=  $this->Crop_pest_model->cropDetails3($uni_id,$uploader_id);
            $data['taxo4']=  $this->Crop_pest_model->cropDetails4($uni_id,$uploader_id);
            $data['taxo5']=  $this->Crop_pest_model->cropDetails5($uni_id,$uploader_id);
            $data['taxo6']=  $this->Crop_pest_model->cropDetails6($uni_id,$uploader_id);
            $data['taxo7']=  $this->Crop_pest_model->cropDetails7($uni_id,$uploader_id);
            $data['taxo8']=  $this->Crop_pest_model->cropDetails8($uni_id,$uploader_id);
            $data['taxo9']=  $this->Crop_pest_model->cropDetails9($uni_id,$uploader_id);
            $data['taxo10']=  $this->Crop_pest_model->cropDetails10($uni_id,$uploader_id);
            $data['taxo11']=  $this->Crop_pest_model->cropDetails11($uni_id,$uploader_id);
            $data['taxo12']=  $this->Crop_pest_model->cropDetails12($uni_id,$uploader_id);
            $data['taxo13']=  $this->Crop_pest_model->cropDetails13($uni_id,$uploader_id);
            //print_r($this->Crop_pest_model->cropDetails(1));
            $data['view'] = 'crop/crop_details';
            $this->load->view('layout', $data);
        }
        else{
            redirect('admin/auth/login');
        }

    }
}