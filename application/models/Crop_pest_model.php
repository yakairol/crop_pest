<?php
    class Crop_pest_model extends CI_Model
    {
        function cropDetails($uni_id,$uploader_id)
        {
            $this->db->where('taxonomy_uni_id', $uni_id);
            $this->db->where('taxonomy_uploader', $uploader_id);
            $query = $this->db->get('taxonomy');
            return $query->row_array();
        }
        function cropDetails2($uni_id,$uploader_id)
        {
            $this->db->where('taxonomy_name_uni_id', $uni_id);
            $this->db->where('taxonomy_name_uploader', $uploader_id);
            $query = $this->db->get('taxonomy_name');
            return $query->result();
        }
        function cropDetails3($uni_id,$uploader_id)
        {
            $this->db->where('taxonomy_syn_uni_id', $uni_id);
            $this->db->where('taxonomy_syn_uploader', $uploader_id);
            $query = $this->db->get('taxonomy_syn');
            return $query->result();
        }
        function cropDetails4($uni_id,$uploader_id)
        {
            $this->db->where('pest_det_uni_id', $uni_id);
            $this->db->where('pest_det_uploader', $uploader_id);
            $query = $this->db->get('crop_pest_details');
            return $query->row_array();
        }
        function cropDetails5($uni_id,$uploader_id)
        {
            $this->db->where('paper_reference_uni_id', $uni_id);
            $this->db->where('paper_reference_uploader', $uploader_id);
            $query = $this->db->get('paper_reference');
            return $query->row_array();
        }
        function cropDetails6($uni_id,$uploader_id)
        {
            $this->db->where('distribution_uni_id', $uni_id);
            $this->db->where('distribution_uploader', $uploader_id);
            $query = $this->db->get('distribution');
            return $query->row_array();
        }
        function cropDetails7($uni_id,$uploader_id)
        {
            $this->db->where('natural_history_uni_id', $uni_id);
            $this->db->where('natural_history_uploader', $uploader_id);
            $query = $this->db->get('natural_history');
            return $query->row_array();
        }
        function cropDetails8($uni_id,$uploader_id)
        {
            $this->db->where('life_cycle_uni_id', $uni_id);
            $this->db->where('life_cycle_uploader', $uploader_id);
            $query = $this->db->get('life_cycle');
            return $query->row_array();
        }
        function cropDetails9($uni_id,$uploader_id)
        {
            $this->db->where('prevention_uni_id', $uni_id);
            $this->db->where('prevention_uploader', $uploader_id);
            $query = $this->db->get('prevention');
            return $query->row_array();
        }
        function cropDetails10($uni_id,$uploader_id)
        {
            $this->db->where('parasite_uni_id', $uni_id);
            $this->db->where('parasite_uploader', $uploader_id);
            $query = $this->db->get('parasite');
            return $query->result();
        }
        function cropDetails11($uni_id,$uploader_id)
        {
            $this->db->where('pradators_uni_id', $uni_id);
            $this->db->where('pradators_uploader', $uploader_id);
            $query = $this->db->get('pradators');
            return $query->result();
        }
        function cropDetails12($uni_id,$uploader_id)
        {
            $this->db->where('yield_loss_uni_id	', $uni_id);
            $this->db->where('yield_loss_uploader', $uploader_id);
            $query = $this->db->get('yield_loss');
            return $query->result();
        }
        function cropDetails13($uni_id,$uploader_id)
        {
            $this->db->where('add_info_uni_id', $uni_id);
            $this->db->where('add_info_uploader', $uploader_id);
            $query = $this->db->get('add_info');
            return $query->result();
        }

    }
?>