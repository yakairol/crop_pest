<?php
	class Auth_model extends CI_Model{

        public function login($data){
            $query = $this->db->get_where('ci_users', array('email' => $data['email']));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.
                $result = $query->row_array();
                $validPassword = password_verify($data['password'], $result['password']);
                if($validPassword){
                    //$this->db->select('*');
                    $this->db->from('ci_users');
                    $this->db->join('role_type','role_type.role_type_name = ci_users.is_admin');
                    $this->db->where('email', $data['email']);
                    $query2 = $this->db->get();
                    //return $query->result();
                    return $result = $query2->row_array();
                }

            }
        }

		public function change_pwd($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_users', $data);
			return true;
		}
        public function selectprofile($id){
            $this->db->where("id",$id);
            $query=$this->db->get('ci_users');
            return $query->result();
        }

	}

?>