<?php
	class User_model extends CI_Model{

		public function add_user($data){
			$this->db->insert('ci_users', $data);
			return true;
		}

		public function get_all_users(){
        $this->db->select('*');
        $query = $this->db->get('ci_users');
        //return $result = $query->result_array();
        return $query->result();
        }
        public function get_all_users2($id){
            $this->db->select('*');
            $this->db->where('email',$id);
            $query = $this->db->get('ci_users');
            //return $result = $query->result_array();
            return $query->result();
        }

        public function edit_pp($data, $id){
            $this->db->where('id', $id);
            $this->db->update('ci_users', $data);
            return true;
        }
		public function get_user_by_id($id){
			$query = $this->db->get_where('ci_users', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_user($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_users', $data);
			return true;
		}
        function delete_user($id)
        {
            $this->db->where('id', $id);
            $this->db->delete("ci_users");
            //DELETE FROM users WHERE id = '$user_id'
        }

		public function get_role_type(){
            $this->db->select('*');
            $query = $this->db->get('role_type');
            return $query->result();
        }

        public function set_role_type_permission($role_type,$data){
            $this->db->select('*');
            $this->db->where('role_type_name', $role_type);
            $this->db->update('role_type', $data);
            return true;
        }

        function add_role_type($data){
            $this->db->insert("role_type", $data);
        }
        function fetchRole($id){
            $this->db->where("role_id",$id);
            $query=$this->db->get('role_type');
            return $query->result();
        }
        function updateRole($id,$role){
            $this->db->where("role_id",$id);
            $this->db->update('role_type',$role);
        }
        function deleteRole($id){
            $this->db->where("role_id",$id);
            $this->db->delete('role_type');
        }
        public function get_page_link(){
            $this->db->select('*');
            $query = $this->db->get('set_permission');
            return $query->result();
        }

        function user_last_dat($post_data) {
            $this->db->insert('ci_users',$post_data);
            return $this->db->insert_id();
        }


        public function PictureUrlById($id)
        {
            if($this->session->userdata['role']=='Patient'){
                $this->db->select('online_registration_id,photo');
                $this->db->from('online_registration');
                $this->db->where("online_registration_id", $id);
                $this->db->limit(1);
                $query = $this->db->get();
                $res = $query->row_array();
                if (!empty($res['photo'])) {
                    return base_url('uploads/online_registration/' . $res['photo']);
                } else {
                    return base_url('uploads/images/user-icon.jpg');
                }
            }else{
                $this->db->select('id,profile_pic');
                $this->db->from('ci_users');
                $this->db->where("id", $id);
                $this->db->limit(1);
                $query = $this->db->get();
                $res = $query->row_array();
                if (!empty($res['profile_pic'])) {
                    return base_url('uploads/images/' . $res['profile_pic']);
                } else {
                    return base_url('uploads/images/user-icon.jpg');
                }
            }

        }

        public function GetName($id)
        {
            if($this->session->userdata['role']=='Patient'){
                $this->db->select('online_registration_id,full_name');
                $this->db->from('online_registration');
                $this->db->where("online_registration_id", $id);
                $this->db->limit(1);
                $query = $this->db->get();
                $res = $query->row_array();
                return $res['full_name'];
            }else{
                $this->db->select('id,username');
                $this->db->from('ci_users');
                $this->db->where("id", $id);
                $this->db->limit(1);
                $query = $this->db->get();
                $res = $query->row_array();
                return $res['username'];
            }


        }

        public function PictureUrl()
        {
            if($this->session->userdata['role']=='Patient'){
                $this->db->select('online_registration_id,photo');
                $this->db->from('online_registration');
                $this->db->where("online_registration_id", $this->session->userdata['admin_id']);
                $this->db->limit(1);
                $query = $this->db->get();
                $res = $query->row_array();
                if (!empty($res['photo'])) {
                    return base_url('uploads/online_registration/' . $res['photo']);
                } else {
                    return base_url('uploads/images/user-icon.jpg');
                }
            }else{
                $this->db->select('id,profile_pic');
                $this->db->from('ci_users');
                $this->db->where("id", $this->session->userdata['admin_id']);
                $this->db->limit(1);
                $query = $this->db->get();
                $res = $query->row_array();
                if (!empty($res['picture_url'])) {
                    return base_url('uploads/images/' . $res['picture_url']);
                } else {
                    return base_url('uploads/images/user-icon.jpg');
                }
            }

        }
        public function GetUserData()
        {
            if($this->session->userdata['role']=='Patient'){
                $this->db->select('*');
                $this->db->from('online_registration');
                $this->db->where("online_registration_id", $this->session->userdata['admin_id']);
                $this->db->limit(1);
                $query = $this->db->get();
                if ($query) {
                    return $query->row_array();
                } else {
                    return false;
                }
            }else{
                $this->db->select('*');
                $this->db->from('ci_users');
                $this->db->where("id", $this->session->userdata['admin_id']);
                $this->db->limit(1);
                $query = $this->db->get();
                if ($query) {
                    return $query->row_array();
                } else {
                    return false;
                }
            }
        }

	}

?>