<?php
	class Crop_pest_model_add extends CI_Model{
        public function pest_det($data){
            $this->db->insert('crop_pest_details', $data);
            return true;
        }

        public function paper_reference($data){
            $this->db->insert('paper_reference', $data);
            return true;
        }

        public function taxonomy_syn($data){
            $this->db->insert('taxonomy_syn', $data);
            return true;
        }

        public function taxonomy($data){
            $this->db->insert('taxonomy', $data);
            return true;
        }

        public function taxonomy_name($data){
            $this->db->insert('taxonomy_name', $data);
            return true;
        }

        public function natural_history($data){
            $this->db->insert('natural_history', $data);
            return true;
        }

        public function distribution($data){
            $this->db->insert('distribution', $data);
            return true;
        }

        public function life_cycle($data){
            $this->db->insert('life_cycle', $data);
            return true;
        }


        public function prevention($data){
            $this->db->insert('prevention', $data);
            return true;
        }

        public function parasite($data){
            $this->db->insert('parasite', $data);
            return true;
        }

        public function pradators($data){
            $this->db->insert('pradators', $data);
            return true;
        }


        public function yield_loss($data){
            $this->db->insert('yield_loss', $data);
            return true;
        }

        public function add_info($data){
            $this->db->insert('add_info', $data);
            return true;
        }

        function general_image($data){
            $this->db->insert('general_images', $data);
            return true;
        }

        function symtom_image($data){
            $this->db->insert('symtom_images', $data);
            return true;
        }

        function lifecycle_image($data){
            $this->db->insert('ife_cycle_images', $data);
            return true;
        }



	}

?>