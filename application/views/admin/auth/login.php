<!DOCTYPE html>
<html lang="en">
<head>
    <title>Crop Pest</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->

    <!-------- common for every page ----------->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/><!-- for fontsize stylling -->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/bootstrap.css" />
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/materialadmin.css?1425466319" /> <!-- css for material admin-->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/font-awesome.min.css?1422529194" /> <!-- side bar icon-->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/material-design-iconic-font.min.css?1421434286" /><!-- side bar icon-->

    <!----- For datatable ---------->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989" />
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990" />
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990" /><!-- for printing -->

    <!--- For Select ----->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/select2/select2.css?1424887856" /><!--for select -->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/multi-select/multi-select.css?1424887857" /><!--for multi select -->

    <!-- For Datepicker --->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858" /><!-- for datepicker -->

    <!---- my custom css -->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/custom.css" /> <!-- custom css -->

    <!--- for printing ------>
    <link type="text/css" rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/materialadmin_print.css?1419847669"  media="print"/>

    <!-- END STYLESHEETS -->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
</head>
<body class="menubar-hoverable header-fixed ">

<!-- BEGIN LOGIN SECTION -->
<section class="section-account">
    <div class="img-backdrop" style="background-image: url('<?= base_url() ?>public/assets//img/img16.jpg')"></div>
    <div class="spacer"></div>

    <div class="card contain-sm style-transparent">
        <div class="card-body pt-2">
            <div class="row">
                <?php if($this->session->flashdata('msg') !=''){ ?>
                    <div id="alert" class="col-md-offset-3 col-md-6" style="margin-top:5px;">
                        <a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction"><i class="md md-notifications pull-right text-success"></i>    <?php echo $this->session->flashdata('msg'); ?></a>
                    </div>
                <?php } ?>
                <div class="col-sm-6 col-md-offset-3">
                    <br/>
                    <span class="text-lg text-bold text-primary">CROP PEST </span>
                    <br/><br/>
                    <?php echo form_open(base_url('admin/auth/login'), 'class="form floating-label"'); ?>
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" name="email">
                            <label for="email">Email/Phone Number</label>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password">
                            <label for="password">Password</label>
                            <p class="help-block"><a href="#"></a></p>
                        </div>
                        <br/>
                        <div class="row">
                            <!--div class="col-xs-6 text-left">
                                <div class="checkbox checkbox-inline checkbox-styled">
                                    <label>
                                    </label>
                                </div>
                            </div--><!--end .col -->
                            <div class="col-md-12 col-sm-12 mb-2">
                                <button class="btn btn-primary btn-block ink-reaction btn-raised" name="submit" id="submit" type="submit" value="Submit">Login</button>
                            </div><!--end .col -->
                            <div class="col-md-12 col-sm-12 mb-2">
                                <a href="#" class="btn btn-primary btn-block ink-reaction btn-raised" >New Registration</a>
                            </div><!--end .col -->
                            <div class="col-md-12 col-sm-12 mt-3">
                              <div class="btn-group btn-group-justified" role="group" aria-label="Justified button group">
                                  <a class="btn btn-primary-light" role="button"><i class="fa fa-facebook"></i> Facebook</a>
                                  <a class="btn btn-warning" role="button"><i class="fa fa-google"></i> Google</a>
                                  <a class="btn btn-info" role="button"><i class="fa fa-twitter"></i> Twitter</a>
                              </div>
                            </div>
                        </div><!--end .row -->
                   <!-- </form>-->
                    <?php echo form_close(); ?>
                </div><!--end .col -->

            </div><!--end .row -->
        </div><!--end .card-body -->
    </div><!--end .card -->
</section>
<!-- END LOGIN SECTION -->

<!-- BEGIN JAVASCRIPT -->
<!-- BEGIN JAVASCRIPT -->


<!-------- common for every page ----------->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/bootstrap/bootstrap.min.js"></script> <!-- for header navigation -->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/App.js"></script><!-- menubar data toogle minimize maximize-->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/AppNavigation.js"></script><!-- menubar data toogle minimize maximize-->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/AppOffcanvas.js"></script> <!-- right side comvas -->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/AppNavSearch.js"></script><!-- for navigation bar search -->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script> <!-- for sidebar dropdown -->

<!-- for datatable page -------------------->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/DataTables/jquery.dataTables.min.js"></script><!-- for datable-->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script><!-- for datatable colvis-->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script><!-- for datatable print-->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/demo/DemoTableDynamic.js"></script>

<!------- For select --------------->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/select2/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/demo/DemoFormComponents.js"></script>

<!-- for date picker ------>
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/demo/DemoFormComponents.js"></script>

<!---- for form with select ----------->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/AppForm.js"></script>

<!-- for form validation -->
<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>

<!-- for data tongle tooltip---->
<!--<script src="<?//= base_url() ?>public/assets/js/libs/jquery/jquery-1.11.2.min.js"></script>-->
<script src="<?= base_url() ?>public/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>

<!------  for tabs navigation ---->
<script src="<?= base_url() ?>public/assets/js/core/source/AppVendor.js"></script>



<!-- END JAVASCRIPT -->
</body>

<script>
    $(document).ready(function() {
        setTimeout(function() {
            $('#toast-success').fadeOut('fast');
        }, 2000); // <-- time in milliseconds
    });
</script>