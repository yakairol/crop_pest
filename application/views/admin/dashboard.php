<div class="section-body">
    <div class="row">
        <div class="col-lg-offset-0 col-md-12">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-info no-margin">
                                    <h1 class="pull-right text-info"><i class="fa fa-wheelchair"></i></h1>
                                    <strong class="text-xl"><?php echo "testing"; ?></strong><br/>
                                    <span class="opacity-50">Total Patients</span>
                                    <div class="stick-bottom-left-right">
                                        <div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
                                    </div>
                                </div>
                            </div><!--end .card-body -->
                        </div><!--end .card -->
                    </div><!--end .col -->
                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-warning no-margin">
                                    <h1 class="pull-right text-warning"><i class="fa fa-eyedropper"></i></h1>
                                    <!--<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>-->
                                    <strong class="text-xl"><?php echo "testing"; ?></strong><br/>
                                    <span class="opacity-50">IPD Patient</span>
                                    <div class="stick-bottom-right">
                                        <div class="height-1 sparkline-visits" data-bar-color="#e5e6e6"></div>
                                    </div>
                                </div>
                            </div><!--end .card-body -->
                        </div><!--end .card -->
                    </div><!--end .col -->
                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-danger no-margin">
                                    <h1 class="pull-right text-danger"><i class="md md-today"></i></h1>
                                    <!--<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>-->
                                    <strong class="text-xl"><?php
                                        echo "testing";
                                        ?></strong><br/>
                                    <span class="opacity-50">Appointments</span>
                                    <div class="stick-bottom-left-right">
                                        <div class="height-1 sparkline-visits" data-bar-color="#e5e6e6"></div>
                                    </div>
                                </div>
                            </div><!--end .card-body -->
                        </div><!--end .card -->
                    </div><!--end .col -->
                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-body no-padding">
                                <div class="alert alert-callout alert-success no-margin">
                                    <h1 class="pull-right text-success"><i class="fa fa-user-md"></i></h1>
                                    <strong class="text-xl"><?php
                                        echo "testing";
                                        ?></strong><br/>
                                    <span class="opacity-50">Doctors</span>
                                </div>
                            </div><!--end .card-body -->
                        </div><!--end .card -->
                    </div><!--end .col -->
                </div>
            </div><!--end .col -->
    </div>

</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

