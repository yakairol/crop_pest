<div class="section-body">
    <div class="row">
        <div class="col-lg-offset-0 col-md-12">
            <div class="card">
                <!--<div class="form-group">-->
                <form  class="form form-validate floating-label" action="" id="application_setting3" >
                    <div class="card-head style-primary">
                        <div class="col-lg-6">
                            <header style ="padding:11px 0px;"><i class="fa fa-gears"></i> Application Setting </header>
                        </div>
                        <div class="tools">
                            <div class="btn-group">
                               <span>
                                    <input type="submit" name="submit"  value="Update Profile " class="btn ink-reaction btn-raised btn-default-light">
                                </span>
                                <span>
                                     <button type="button" class="btn ink-reaction btn-raised btn-default-light" onclick='window.location.reload(true);'">Cancel</button>
                                </span>
                            </div>

                        </div>
                    </div>
                    <div class="card-head style-default-light">
                        <ul class="nav nav-tabs" data-toggle="tabs">
                            <li class="active"><a href="#first1"><i class="fa fa-sign-in"></i> Log In Page</a></li>
                            <li><a href="#first2"><i class="fa fa-tachometer"></i> Dashboard Setting</a></li>
                            <li><a href="#first3"><i class="fa fa-gear"></i> General Setting</a></li>
                        </ul>
                    </div><!--end .card-head -->
                    <div class="card-body tab-content">
                        <div class="tab-pane active" id="first1">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="pageHeader" name="pageHeader" data-rule-minlength="1" maxlength="100"  aria-required="true" value="<?= $hospital_setting['log_in_header']; ?>">
                                    <label for="pageHeader">Log In Page Header <span style="color: red">*</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="first2">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="dashboardHeader" name="dashboardHeader" data-rule-minlength="1" maxlength="100"  aria-required="true" value="<?= $hospital_setting['dashboard_header']; ?>">
                                        <label for="dashboardHeader">Dashboard Main Header <span style="color: red">*</span></label>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="dashboardAddress" name="dashboardAddress" data-rule-minlength="1" maxlength="100"  aria-required="true" value="<?= $hospital_setting['dashboard_address']; ?>">
                                        <label for="dashboardAddress">Dashboard Header Address <span style="color: red">*</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <?php
                                        echo '<img  id="logo"  src="'.base_url().'uploads/images/'. $hospital_setting['logo'].'" width="200" height="" class="img-thumbnail" alt="Logo is Empty"/>';
                                        ?>
                                        <label for="uploadLogo">Change Logo  <span >( jpg,jpeg,gif,png )</span> </label>
                                        <input type="file" class="form-control" id="uploadLogo" name="uploadLogo"/>
                                        <input  hidden  id="uploadLogo2" name="uploadLogo2" value="<?php echo $hospital_setting['logo']; ?>" />

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="first3">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="phcEmail" name="phcEmail" data-rule-minlength="1" maxlength="100"  aria-required="true" value="<?= $hospital_setting['phc_email']; ?>">
                                        <label for="phcEmail">PHC Email <span style="color: red">*</span></label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="mobileNumber" name="mobileNumber" data-rule-minlength="10" maxlength="10" required="" aria-required="true" min="1000000000" value="<?= $hospital_setting['phc_phone']; ?>">
                                        <label for="mobileNumber">Mobile Number <span style="color: red">*</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="remark" name="remark" data-rule-minlength="1" maxlength="100"  aria-required="true" value="<?= $hospital_setting['phc_remark']; ?>">
                                        <label for="remark">Remark <span style="color: red">*</span></label>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $(document).on('submit','#application_setting3', function (event) {
            event.preventDefault();
            $link = $(this);
            $.ajax({
                url:"<?php echo base_url(). 'admin/Setting/updateSetting';?>",
                method: 'POST',
                data:new FormData(this),
                contentType:false,
                processData:false,
                success:function (data) {
                    alert("Successfully updated.!! ");
                    window.location.reload();
                }
            });
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#logo').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }
        $("#uploadLogo").change(function() {
            readURL(this);
        });

    });
</script>