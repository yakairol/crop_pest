<div class="section-body">
    <div class="row">
        <div class="col-lg-offset-0 col-md-12">

            <div class="card">
                <!--<div class="form-group">-->
                <?php echo form_open_multipart(base_url('admin/users/set_permission'), 'class="form-group"');  ?>
                <div class="card-head style-primary">
                    <header>Set Permissions</header>
                    <div class="tools">
                        <div class="btn-group">
                            <button type="button" class="btn ink-reaction btn-raised btn-default-light" onclick='window.location.reload(true);'">Cancel</button>
                            <input type="submit" name="submit"  value="Save" class="btn ink-reaction btn-raised btn-default-light">
                            <!--<input type="hidden" name="Page 1" id=" ">
                            <input type="submit" name="submit"  value="Save" class="btn ink-reaction btn-raised btn-default-light">-->
                        </div>

                    </div>
                </div>
                <?php if($this->session->flashdata('msg') !=''){ ?>
                    <div id="alert" class="col-md-offset-3 col-md-6" style="margin-top:5px;">
                    <a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction"><i class="md md-notifications pull-right text-success"></i>    <?php echo $this->session->flashdata('msg'); ?></a>
                    </div>
                <?php } ?>
                    <!--<div class="col-lg-offset-0 col-md-12">
                        <h4>Set Permission of Module for different Role</h4>
                    </div><!--end .col -->
                    <!--<div class="col-lg-offset-0 col-md-12">-->
                   <div class="col-lg-12">
                        <!--<div class="card-body floating-label">-->
                        <!-- <div class="table-responsive" style="margin-top:10px;">-->
                        <div class="table-responsive" style="margin-top:5px;">
                            <table class="table table-striped" >
                                <tr>
                                    <td style="width:150px;"><b>Module Name</b>
                                    </td>
                                    <?php $i = 0; ?>
                                    <?php foreach($get_user_role as $row): ?>
                                        <?php if($row->role_type_name != 'admin') { ?>
                                            <td>
                                                            <!--<td><?php// echo $data; ?></td>-->
                                             <input type="text" readonly="readonly" name="role[]" style="font-size:14px;" value="<?php echo $row->role_type_name; ?>" class="form-control">
                                            </td>
                                        <?php $i = $i + 1; ?>
                                    <?php } ?>
                                    <?php  endforeach; ?>
                                </tr>
                                    <!------------ appointment module--------->
                                    <tr>
                                        <td><b>Crop Pest</b></td>
                                    </tr>
                                    <tr>
                                        <td >Add Crop Pest</td>
                                        <?php $i = 0; ?>
                                        <?php foreach($get_user_role as $row): ?>
                                            <?php if($row->role_type_name != 'admin') { ?>
                                                <td>
                                                    <label class="checkbox-inline checkbox-styled">
                                                        <input id="cb3"  type="checkbox" name="add_appointment[<?php echo $i?>]" value="1" <?php if($row->add_crop_pest	 == 1){echo "checked";}?> ><span></span>
                                                    </label>
                                                </td>
                                                <?php $i = $i + 1; ?>
                                            <?php } ?>
                                        <?php  endforeach; ?>
                                    </tr>
                                <tr>
                                    <td >View Crop Pest</td>
                                    <?php $i = 0; ?>
                                    <?php foreach($get_user_role as $row): ?>
                                        <?php if($row->role_type_name != 'admin') { ?>
                                            <td>
                                                <label class="checkbox-inline checkbox-styled">
                                                    <input id="cb3"  type="checkbox" name="todays_appointment[<?php echo $i?>]" value="1" <?php if($row->view_crop_pest == 1){echo "checked";}?> ><span></span>
                                                </label>
                                            </td>
                                            <?php $i = $i + 1; ?>
                                        <?php } ?>
                                    <?php  endforeach; ?>
                                </tr>
                                <tr>
                                    <td><b> Manage Users </b></td>
                                </tr>
                                <tr>
                                    <td> Views Users </td>
                                    <?php $i = 0; ?>
                                    <?php foreach($get_user_role as $row): ?>
                                        <?php if($row->role_type_name != 'admin') { ?>
                                            <td>
                                                <label class="checkbox-inline checkbox-styled">
                                                    <input id="cb3"  type="checkbox" name="view_users[<?php echo $i?>]" value="1" <?php if($row->view_users == 1){echo "checked";}?> ><span></span>
                                                </label>
                                            </td>
                                            <?php $i = $i + 1; ?>
                                        <?php } ?>
                                    <?php  endforeach; ?>
                                </tr>
                                <tr>
                                    <td> Add Users </td>
                                    <?php $i = 0; ?>
                                    <?php foreach($get_user_role as $row): ?>
                                        <?php if($row->role_type_name != 'admin') { ?>
                                            <td>
                                                <label class="checkbox-inline checkbox-styled">
                                                    <input id="cb3"  type="checkbox" name="add_users[<?php echo $i?>]" value="1" <?php if($row->add_users == 1){echo "checked";}?> ><span></span>
                                                </label>
                                            </td>
                                            <?php $i = $i + 1; ?>
                                        <?php } ?>
                                    <?php  endforeach; ?>
                                </tr>
                                <tr>
                                    <td> Add Role </td>
                                    <?php $i = 0; ?>
                                    <?php foreach($get_user_role as $row): ?>
                                        <?php if($row->role_type_name != 'admin') { ?>
                                            <td>
                                                <label class="checkbox-inline checkbox-styled">
                                                    <input id="cb3"  type="checkbox" name="add_roles[<?php echo $i?>]" value="1" <?php if($row->add_roles == 1){echo "checked";}?> ><span></span>
                                                </label>
                                            </td>
                                            <?php $i = $i + 1; ?>
                                        <?php } ?>
                                    <?php  endforeach; ?>
                                </tr>
                                <tr>
                                    <td> Add Permissions </td>
                                    <?php $i = 0; ?>
                                    <?php foreach($get_user_role as $row): ?>
                                        <?php if($row->role_type_name != 'admin') { ?>
                                            <td>
                                                <label class="checkbox-inline checkbox-styled">
                                                    <input id="cb3"  type="checkbox" name="add_permission[<?php echo $i?>]" value="1" <?php if($row->add_permission == 1){echo "checked";}?> ><span></span>
                                                </label>
                                            </td>
                                            <?php $i = $i + 1; ?>
                                        <?php } ?>
                                    <?php  endforeach; ?>
                                </tr>
                                </table>
                        </div><!--end .table-responsive -->

                    </div><!--end .card -->

                <!--<input type="hidden" name="<?php// echo $data; ?>" id=" ">-->
                <!-- <input type="submit" name="submit"  value="Save" class="btn ink-reaction btn-raised btn-default-light">-->
                <?php echo form_close( ); ?>
                <!--</div><!--end .row -->
            </div>
        </div>
    </div>
</div>

<script>
    /*$(document).ready(function(){
        $('#cb1').on('change', function(){
            this.value = this.checked ? 1 : 0;
            alert(this.value);
        }).change();
        $('#cb2').on('change', function(){
            this.value = this.checked ? 1 : 0;
            alert(this.value);
        }).change();
        $('#cb3').on('change', function(){
            this.value = this.checked ? 1 : 0;
            alert(this.value);
        }).change();
        $('#cb4').on('change', function(){
            this.value = this.checked ? 1 : 0;
            alert(this.value);
        }).change();

       /* $('input[type="checkbox"]').change(function(){
            this.value = (Number(this.checked));
        });*/
    /* });*/
    setTimeout(function() {
        $('#alert').fadeOut('fast');
    }, 2000); // <-- time in milliseconds
</script>