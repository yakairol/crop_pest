<div class="section-body">
    <div class="row">
        <div class="col-lg-offset-0 col-md-12">

            <div class="card">
                <!--<div class="form-group">-->
                <div class="card-head style-primary">
                    <div class="col-lg-4">
                        <header style ="padding:11px 0px;"><i class="fa fa-users"></i> Role List</header>
                    </div>
                    <!--<div class="col-lg-2" style="margin-left:280px;">
                        <a id="selectTriggerFilter"></a>
                    </div>
                    <div class="col-lg-2" style="">
                        <a id="selectTriggerFilter1"></a>
                    </div>-->
                    <div class="tools">
                        <div class="btn-group">
                            <span data-toggle="tooltip"  data-placement="left"  data-original-title="Add New Role">
                                <a class="btn btn-icon-toggle btn-floating-action" style="color:black;background-color: #E8E8E8	;"  data-toggle="modal"  data-target="#myModal" ><i class="fa fa-plus"></i></a>
                            </span>
                        </div>

                    </div>
                </div>

                <div id="alertTable" class="col-md-offset-2 col-md-6 col-lg-8" style="margin-top:5px;display: none;">
                </div>
                <!--<div class="col-lg-offset-0 col-md-12">-->
                <div class="col-lg-12">
                    <div class="table-responsive" style="margin-top:5px;">
                        <table id="datatable12" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th style="width:20%;">#</th>
                                <th style="width:50%;">Role Type</th>
                                <th style="width:30%;">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <!--<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                            Tooltip on top
                        </button>-->
                    </div><!--end .table-responsive -->
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal" id="mylabModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <!-- <div class="modal-backdrop fade in" style="height: 451px;"></div>-->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="formModalLabel"><i class="fa fa-edit"></i> Edit Role Name</h4>
            </div>
            <div class="col-md-offset-0 col-md-12" id="alertEdit" style="margin-top:5px;display: none;">
            </div>
            <form id="edit_role_form" class="form-horizontal" role="form">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="editRoleName" class="control-label">Role Name</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="editRoleName" id="editRoleName" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" id="user_id">
                    <input type="submit" class="btn btn-primary" name="action" value="Save" id="action" >
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div>

<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <!-- <div class="modal-backdrop fade in" style="height: 451px;"></div>-->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="formModalLabel">Add New Role</h4>
            </div>
            <div class="col-md-offset-0 col-md-12" id="alertAdd" style="margin-top:5px;display: none;">
            </div>
            <form id="add_role" class="form-horizontal" role="form">

                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label for="roleName" class="control-label">Role Name</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="roleName" id="roleName" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<input type="submit" class="btn btn-success" name="action" value="ADD" id="action" >
                    <button style="margin-right: 15px;" type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->

                    <input type="submit" class="btn btn-primary" name="action" value="ADD">
                    <!-- <button  name="action" value="Save" id="action"  type="button" class="btn btn-primary">Save</button>-->
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div>

<div class="modal" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="formModalLabel"><i class="fa fa-trash"></i> Delete Lab Category</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-10">
                        <label for="catName" class="control-label"> Are you sure you want to delete this record?</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnDelete" class="btn btn-default" data-dismiss="modal">Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<style>
    .dataTables_wrapper .dt-buttons {
        float:right;
        /*padding-left: 12px;*/
    }
    /*@media print {
        table td:last-child {display:none}
        table th:last-child {display:none}
    }
    tfoot {
        display: table-header-group;
    }*/
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!--<script src="https://code.jquery.com/jquery-3.5.1.js" type="text/javascript" language="javascript"></script>-->
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
<!--<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js" type="text/javascript" language="javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js" type="text/javascript" language="javascript"></script>
<script>
    $(document).ready(function() {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
        var dataTable = $('#datatable12').DataTable({

            //"dom": 'lBfrtip',
            "dom": 'lBCfrtip',
            //dom: '1BCfrtip',
            /*buttons: [
                //'copy', 'csv', 'excel', 'pdf', 'print'
                'excel', 'pdf', 'print'

            ],*/
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "order": [],
            //"dom": 'lCfrtip', //  l = entries,C = column (COVIES) ,f = search bar,  p = pagination in abpve, tp = pagination below,i = shpwing 1 to 6
            buttons: [
                {
                    name: 'print',
                    extend: "print",
                    title: 'Role List',
                    //className: 'btn btn-success fa fa-print',
                    exportOptions: {
                        columns: [0, 1]
                    },

                },
                {
                    name: 'excelHtml5',
                    extend: 'excelHtml5',
                    title: 'Role List',
                    //className: 'btn btn-primary fa fa-file-excel',
                    exportOptions: {
                        columns: [0, 1]
                    }
                },
                {
                    extend: 'pdf',
                    title: 'Role List',
                    customize: function (doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        },
                        columns: [0, 1]
                    },
                }
            ],
            initComplete: function () {
                var column = this.api().column(1);
                var select = $('<select style="font-size:13px;" class="form-control select2-list"><option value="">Patient Name</option></select>')
                    .appendTo('#selectTriggerFilter')
                    .on('change', function () {
                        var val = $(this).val();
                        column.search(val ? '^' + $(this).val() + '$' : val, true, false).draw();
                    });

                var column1 = this.api().column(2);
                var select1 = $('<select style="font-size:13px;" class="form-control select2-list"><option value="">Gender</option></select>')
                    .appendTo('#selectTriggerFilter1')
                    .on('change', function () {
                        var val = $(this).val();
                        column1.search(val ? '^' + $(this).val() + '$' : val, true, false).draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
                column1.data().unique().sort().each(function (d, j) {
                    select1.append('<option value="' + d + '">' + d + '</option>');
                });
            },
            "colVis": {
                //"buttonText": "Columns",
                "buttonText": "Filter Columns",
                //"overlayFade": 0,
                //"align": "left",
            },
            "language": {
                "lengthMenu": '_MENU_ entries per page',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "ajax": {
                url: "<?php echo base_url() . 'admin/Users/add_role_list';?>",
                type: "POST",

            },
            "columnDefs": [
                {
                    "orderable": false,

                }
            ]
        });

        $(document).on('submit','#add_role', function (event) {

            event.preventDefault();

            var addcatNamelist= $('#roleName').val().trim();
            if(addcatNamelist !=''  )
            {
                $.ajax({
                    url:"<?php echo base_url(). 'admin/Users/add_role';?>",
                    method: 'POST',
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function (data) {
                        $('#add_role')[0].reset();
                        $('#myModal').modal('hide');
                        //alert('Role is Added Successfully');
                        $('#alertTable').html('<a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction">' +
                            '<i class="md md-notifications pull-right text-success"></i>' +
                            'Role Added successfully' +
                            '</a>'
                        ).fadeIn().delay(1500).fadeOut('slow');
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                $('#alertAdd').html('<a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction">' +
                    '<i class="md md-notifications pull-right text-success"></i>' +
                    'Fields required' +
                    '</a>'
                ).fadeIn().delay(1500).fadeOut('slow');
            }
        });

        $(document).on('submit','#edit_role_form', function (event) {
            event.preventDefault();
            var roleName = $('#editRoleName').val();

            if(roleName !='')
            {
                $.ajax({
                    url:"<?php echo base_url(). 'admin/Users/updateRole';?>",
                    method: 'POST',
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function (data) {
                        //alert(data);
                        $('#edit_role_form')[0].reset();
                        $('#mylabModal').modal('hide');
                        $('#alertTable').html('<a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction">' +
                            '<i class="md md-notifications pull-right text-success"></i>' +
                            'Role updated Successfully ' +
                            '</a>'
                        ).fadeIn().delay(1000).fadeOut('slow');
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                $('#alertEdit').html('<a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction">' +
                    '<i class="md md-notifications pull-right text-success"></i>' +
                    'All field are required ' +
                    '</a>'
                ).fadeIn().delay(1000).fadeOut('slow');
                //alert("fields required");
            }
        });

        $(document).on('click', '.deleteRole', function(){
            var role_id = $(this).attr("id");
            $('#deletemodal').modal('show');
            $('#btnDelete').unbind().click(function(){
                $.ajax({
                    url:"<?php echo base_url(); ?>admin/Users/deleteRole",
                    method:"POST",
                    data:{role_id:role_id},
                    success:function()
                    {
                        // alert(data);
                        $('#alertTable').html('<a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction">' +
                            '<i class="md md-notifications pull-right text-success"></i>' +
                            'Record  Deleted successfully' +
                            '</a>'
                        ).fadeIn().delay(1000).fadeOut('slow');
                        dataTable.ajax.reload();
                    },
                    error: function(){
                        alert('Error deleting');
                    }
                });
            });

        });


    });
    $(document).on('click','.updateRole',function(){
        var user_id = $(this).attr("id");
        $.ajax({
            url:"<?php echo base_url(). 'admin/Users/fetchRole';?>",
            method:"POST",
            data:{user_id:user_id},
            dataType:"json",
            success: function (data) {
                $('#mylabModal').modal('show');
                $('#editRoleName').val(data.roleName);
                $('#user_id').val(user_id);
                $("#action").val("save");
            }
        })
    });
</script>