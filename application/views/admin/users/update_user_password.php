<div class="section-body">
    <div class="row">
        <div class="col-lg-offset-0 col-md-12">
            <?php
            $id = $this->uri->segment(4);
            ?>
            <div class="card">
                <!--<div class="form-group">-->
                <?php echo form_open_multipart(base_url('admin/Auth/change_pwd'), 'class="form form-validate floating-label"');  ?>
                <!--<form  class="form form-validate floating-label">-->
                    <div class="card-head style-primary">
                        <div class="col-lg-4">
                            <header style ="padding:11px 0px;"><i class="fa fa-edit"></i> Update User Password</header>
                        </div>
                        <div class="tools">
                            <div class="btn-group">
                               <span>
                                    <input type="submit" name="submit"  value="Change Password" class="btn ink-reaction btn-raised btn-default-light">
                                </span>
                                <span>
                            <button type="button" class="btn ink-reaction btn-raised btn-default-light" onclick='window.location.reload(true);'">Cancel</button>
                              </span>
                                <span data-toggle="tooltip"  data-placement="top"  data-original-title="Profile Page">
                                <a class="btn btn-floating-action btn-primary" style="color:black;background-color: #E8E8E8;" href="<?= base_url('admin/Users/userProfile'.'/'.$id) ?>" > <i class="fa fa-user-md"></i></a>
                             </span>
                                <span data-toggle="tooltip"  data-placement="top"  data-original-title="Return Back">
                                <a class="btn btn-floating-action btn-primary" style="color:black;background-color: #E8E8E8;" href="<?= base_url('admin/Users') ?>" > <i class="md md-backspace"></i></a>
                             </span>

                            </div>

                        </div>
                    </div>

                    <?php if($this->session->flashdata('msg') !=''){ ?>
                        <div id="alert" class="col-md-offset-3 col-md-6" style="margin-top:5px;">
                            <a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction"><i class="md md-notifications pull-right text-success"></i>    <?php echo $this->session->flashdata('msg'); ?></a>
                        </div>
                    <?php } ?>

                    <div class="card-body">
                        <input hidden name="idd" id="idd"  value="<?php echo $id?>">
                        <div class="row" >
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input  type="text" class="form-control" id="password" name="password" data-rule-minlength="1" maxlength="50" required="" aria-required="true" ">
                                    <label for="firstName">New Password <span style="color: red">*</span></label>
                                    <!-- <p class="help-block">( Request Admin to Change )</p>-->
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="cpassword" name="cpassword" data-rule-minlength="1" maxlength="50" required="" aria-required="true" ">
                                    <label for="cpassword">Confirm Password <span style="color: red">*</span></label>
                                    <!--<p class="help-block">( Request Admin to Change )</p>-->
                                </div>
                            </div>
                        </div>
                    </div>

                <!--</form>-->
                <?php  echo form_close( ); ?>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        setTimeout(function () {
            $('#toast-success').fadeOut('fast');
        }, 2000); // <-- time in milliseconds
    });
</script>