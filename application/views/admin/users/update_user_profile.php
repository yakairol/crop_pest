<div class="section-body">
    <div class="row">
        <div class="col-lg-offset-0 col-md-12">
            <?php
            $id = $this->uri->segment(4);
            ?>
            <div class="card">
                <!--<div class="form-group">-->
                <form  class="form form-validate floating-label" action="" id="application_setting3" >
                    <div class="card-head style-primary">
                        <div class="col-lg-4">
                            <header style ="padding:11px 0px;"><i class="fa fa-edit"></i> Update User Profile</header>
                        </div>
                        <div class="tools">
                            <div class="btn-group">
                               <span>
                                    <input type="submit" name="submit"  value="Update Profile " class="btn ink-reaction btn-raised btn-default-light">
                                </span>
                                <span>
                            <button type="button" class="btn ink-reaction btn-raised btn-default-light" onclick='window.location.reload(true);'">Cancel</button>
                              </span>
                                <span data-toggle="tooltip"  data-placement="top"  data-original-title="Profile Page">
                                <a class="btn btn-floating-action btn-primary" style="color:black;background-color: #E8E8E8;" href="<?= base_url('admin/Users/userProfile'.'/'.$id) ?>" > <i class="fa fa-user-md"></i></a>
                             </span>
                                <span data-toggle="tooltip"  data-placement="top"  data-original-title="Return Back">
                                <a class="btn btn-floating-action btn-primary" style="color:black;background-color: #E8E8E8;" href="<?= base_url('admin/Users') ?>" > <i class="md md-backspace"></i></a>
                             </span>

                            </div>

                        </div>
                    </div>

                    <div class="card-body">
                        <input hidden name="idd" id="idd"  value="<?php echo $id?>">
                        <div class="row" >
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input readonly type="text" class="form-control" id="firstName" name="firstName" data-rule-minlength="1" maxlength="50" required="" aria-required="true" value="<?= $user_details['firstname']; ?>">
                                    <label for="firstName">First Name <span style="color: red">*</span></label>
                                    <!-- <p class="help-block">( Request Admin to Change )</p>-->
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input readonly type="text" class="form-control" id="LastName" name="LastName" data-rule-minlength="1" maxlength="50" required="" aria-required="true" value="<?= $user_details['lastname']; ?>">
                                    <label for="LastName">Last Name <span style="color: red">*</span></label>
                                    <!--<p class="help-block">( Request Admin to Change )</p>-->
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input readonly type="text" class="form-control" id="LastName" name="userName" data-rule-minlength="1" maxlength="50" required="" aria-required="true" value="<?= $user_details['username']; ?>">
                                    <label for="LastName">User Name <span style="color: red">*</span></label>
                                    <!--<p class="help-block">( Request Admin to Change )</p>-->
                                </div>
                            </div>
                        </div>

                        <div class="row" >
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input readonly type="email" class="form-control" id="email" name="email" required="" aria-required="true" value="<?= $user_details['email']; ?>">
                                    <label for="email">Email <span style="color: red">*</span></label>
                                    <!--<p class="help-block">( Request Admin to Change )</p>-->
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="number" class="form-control" id="mobile_number" name="mobile_number" data-rule-minlength="10" maxlength="10" required="" aria-required="true" min="1000000000" value="<?= $user_details['mobile_no'];  ?>">
                                    <label for="mobile_number">Mobile Number <span style="color: red">*</span></label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control select2-list" data-placeholder="" id="role" name="role" required>
                                        <!-- <select class="form-control" data-placeholder="" id="patType" name="patType" required>-->
                                        <option value="<?= $user_details['is_admin']; ?>" ><?= $user_details['is_admin'];?></option>-->
                                        <?php if(!empty($get_role)) {
                                            foreach ($get_role as $raw) {
                                                if($raw->role_type_name != $user_details['is_admin'] ){
                                                    echo '<option value="' . $raw->role_type_name . '">' . $raw->role_type_name . '</option>';
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                    <label for="role">Role <span style="color: red">*</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <?php
                                    echo '<img  id="logo"  src="'.base_url().'uploads/images/'. $user_details['profile_pic'].'" width="100" height="" class="img-thumbnail" alt="Logo is Empty"/>';
                                    ?>
                                    <label for="uploadLogo">Change Photo <span>( jpg,jpeg,gif,png )</span> </label>
                                    <input type="file" class="form-control" id="uploadLogo" name="uploadLogo"/>
                                    <input  hidden  id="uploadLogo2" name="uploadLogo2" value="<?php echo $user_details['profile_pic']; ?>" />

                                </div>
                            </div>
                        </div>

                    </div>

                </form>
                <?php // echo form_close( ); ?>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $(document).on('submit','#application_setting3', function (event) {
            event.preventDefault();
            $link = $(this);
            $.ajax({
                url:"<?php echo base_url(). 'admin/Users/updateUserProfile2';?>",
                method: 'POST',
                data:new FormData(this),
                contentType:false,
                processData:false,
                success:function (data) {
                    alert("Successfully updated.!! ");
                    window.location.reload();
                }
            });
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#logo').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }
        $("#uploadLogo").change(function() {
            readURL(this);
        });

    });
</script>