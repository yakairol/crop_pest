<div class="section-body">
    <div class="row">
            <div class="col-lg-offset-0 col-md-12">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header>Create an account</header>
                        </div>

                        <?php echo form_open_multipart(base_url('admin/users/add'), 'class="form form-validate floating-label" onsubmit="return checkCoords()"');  ?>
                       <div class="card-body floating-label">
                        <!--<div class="col-lg-offset-0 col-md-12">-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="firstname" class="form-control" id="firstname" required data-rule-minlength="2">
                                        <label for="firstname">First Name</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="lastname" class="form-control" id="lastname" required>
                                        <label for="lastname">Last Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control" id="email" required>
                                        <label for="email">Email</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control" id="password" required data-rule-minlength="5">
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="number" name="mobile_no" class="form-control" id="mobile_no" data-rule-minlength="10" max="9999999999" required>
                                        <label for="mobile_no">Mobile Number</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select name="user_role" class="form-control role_type" id="role_type" required>
                                            <option hidden value="">&nbsp;</option>
                                            <?php if(!empty($get_role)) {
                                                foreach ($get_role as $raw) {
                                                    echo '<option value="' . $raw->role_type_name . '">' . $raw->role_type_name . '</option>';
                                                }
                                            }
                                            ?>

                                           <!-- <option value="">&nbsp;</option>
                                            <option value="admin">Admin</option>
                                            <option value="doctor">Doctor</option>
                                            <option value="nurse">Nurse</option>
                                            <option value="user">User</option>
                                            <option value="laboratory">Laboratory</option>
                                            <option value="pharmacist">Pharmacist</option>-->
                                        </select>
                                        <label for="role">Select Role</label>
                                    </div>
                                </div>
                            </div>

                           <div class="row">
                               <div class="col-sm-6">
                                   <div class="form-group">
                                       <input type="file" name="image" class="form-control-file form-control" id="fileInput">
                                       <input type="hidden" id="x" name="x" />
                                       <input type="hidden" id="y" name="y" />
                                       <input type="hidden" id="w" name="w" />
                                       <input type="hidden" id="h" name="h" />
                                   </div>
                               </div>
                               <div class="col-sm-6">
                                   <div class="form-group">
                                       <div id="preview">
                                           <img id="imagePreview" width="50%" style="display:none;"/>
                                           <!-- <img id="imagePreview" width="30%" style="display:none;"/>-->
                                       </div>
                                   </div>
                               </div>
                           </div>




                       </div><!--end .card-body -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <input type="submit"  name="submit" value="Add User" class="btn btn-flat btn-primary ink-reaction">
                               <!-- <button type="submit"  name="submit" class="btn btn-flat btn-primary ink-reaction">Create account</button>-->
                            </div>
                        </div>

                        <?php echo form_close( ); ?>

                    </div><!--end .card -->

            </div><!--end .col -->
    </div>
</div>
<link rel="stylesheet" href="<?= base_url() ?>public/assets/imageselect/New/css/imgareaselect-default.css">
<script src="<?= base_url() ?>public/assets/imageselect/jquery.imgareaselect.js"></script>
<script>

    function checkCoords(){
        if(parseInt($('#w').val()))
        {
            return true;

        }else{
            alert('Please select a crop region for picture then submit.');
            return false;
        }

    }

    // Set image coordinates
    function updateCoords(im,obj){
        var img = document.getElementById("imagePreview");
        var orgHeight = img.naturalHeight;
        var orgWidth = img.naturalWidth;

        var porcX = orgWidth/im.width;
        var porcY = orgHeight/im.height;

        $('input#x').val(Math.round(obj.x1 * porcX));
        $('input#y').val(Math.round(obj.y1 * porcY));
        $('input#w').val(Math.round(obj.width * porcX));
        $('input#h').val(Math.round(obj.height * porcY));
    }

    $(document).ready(function(){
        // Prepare instant image preview
        $("#crop").hide();
        var p = $("#imagePreview");
        $("#fileInput").change(function(){
            //fadeOut or hide preview
            $('#crop').removeAttr('hidden');
            $("#crop").show();
            p.fadeOut();

            //prepare HTML5 FileReader
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("fileInput").files[0]);

            oFReader.onload = function(oFREvent){
                p.attr('src', oFREvent.target.result).fadeIn();
            };
            $('#imagePreview').imgAreaSelect({
                x1: 101, y1: 60, x2: 166, y2: 125,
                onSelectEnd: updateCoords
            });
        });

        $(document).on('change', '.role_type', function(){
            var selected_role =  $('#role_type').find(":selected").text();
            if(selected_role=='doctor'||selected_role=='Doctor'){
                $('.doc_data').removeAttr('hidden');
            }else{
                console.log(selected_role);
                $(".doc_data").attr("hidden",true);
            }

        });

    });
</script>