<div class="section-body">
    <div class="row">
        <div class="col-lg-offset-0 col-md-12">

            <div class="card">
                <!--<div class="form-group">-->
                <div class="card-head style-primary">
                    <div class="col-lg-4">
                        <header style ="padding:11px 0px;"><i class="fa fa-users"></i>&nbsp;Users List</header>
                    </div>
                    <div class="col-lg-2" style="margin-left:280px;">
                        <a id="selectTriggerFilter"></a>
                    </div>
                    <div class="col-lg-2" style="">
                        <a id="selectTriggerFilter1"></a>
                    </div>
                    <div class="tools">
                        <div class="btn-group">
                            <span data-toggle="tooltip"  data-placement="top"  data-original-title="Add New User">
                                <a href="<?= base_url('admin/Users/add'); ?>"  class="btn btn-icon-toggle btn-floating-action " style="color:black;background-color: #E8E8E8;"  data-toggle="modal"  ><i class="fa fa-plus"></i></i></a>
                            </span>
                        </div>

                    </div>
                </div>

                <div id="alertTable" class="col-md-offset-2 col-md-6 col-lg-8" style="margin-top:5px;display: none;">
                </div>

                <?php if($this->session->flashdata('msg') !=''){ ?>
                    <div id="alert" class="col-md-offset-3 col-md-6" style="margin-top:5px;">
                        <a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction"><i class="md md-notifications pull-right text-success"></i>    <?php echo $this->session->flashdata('msg'); ?></a>
                    </div>
                <?php } ?>

                <!--<div class="col-lg-offset-0 col-md-12">-->
                <div class="col-lg-12">
                    <div class="table-responsive" style="margin-top:5px;">
                        <table id="datatable12" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Mobile No.</th>
                                <th>Role.</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div><!--end .table-responsive -->
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="formModalLabel"><i class="fa fa-trash"></i> Delete User</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-sm-10">
                        <label for="catName" class="control-label"> Are you sure you want to delete this record?</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnDelete" class="btn btn-default" data-dismiss="modal">Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style>
    .dataTables_wrapper .dt-buttons {
        float:right;
        /*padding-left: 12px;*/
    }
    /*@media print {
        table td:last-child {display:none}
        table th:last-child {display:none}
    }
    tfoot {
        display: table-header-group;
    }*/
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!--<script src="https://code.jquery.com/jquery-3.5.1.js" type="text/javascript" language="javascript"></script>-->
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
<!--<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js" type="text/javascript" language="javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js" type="text/javascript" language="javascript"></script>
<script>
    $(document).ready(function() {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        setTimeout(function() {
            $('#toast-success').fadeOut('fast');
        }, 2000); // <-- time in milliseconds

        var dataTable = $('#datatable12').DataTable({
            "dom": 'lBCfrtip',
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "order": [],
            //"dom": 'lCfrtip', //  l = entries,C = column (COVIES) ,f = search bar,  p = pagination in abpve, tp = pagination below,i = shpwing 1 to 6
            buttons: [
                {
                    name: 'print',
                    extend: "print",
                    title: 'User List Details',
                    //className: 'btn btn-success fa fa-print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    },

                },
                {
                    name: 'excelHtml5',
                    extend: 'excelHtml5',
                    title: 'User List Details',
                    //className: 'btn btn-primary fa fa-file-excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
                {
                    extend: 'pdf',
                    title: 'User List Details',
                    customize: function (doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    },
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        },
                        columns: [0, 1, 2, 3, 4]
                    },
                }
            ],
            initComplete: function () {
                var column = this.api().column(0);
                var select = $('<select style="font-size:13px;" class="form-control select2-list"><option value="">First Name</option></select>')
                    .appendTo('#selectTriggerFilter')
                    .on('change', function () {
                        var val = $(this).val();
                        column.search(val ? '^' + $(this).val() + '$' : val, true, false).draw();
                    });

                var column1 = this.api().column(4);
                var select1 = $('<select style="font-size:13px;" class="form-control select2-list"><option value="">Role</option></select>')
                    .appendTo('#selectTriggerFilter1')
                    .on('change', function () {
                        var val = $(this).val();
                        column1.search(val ? '^' + $(this).val() + '$' : val, true, false).draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                });
                column1.data().unique().sort().each(function (d, j) {
                    select1.append('<option value="' + d + '">' + d + '</option>');
                });
            },
            "colVis": {
                //"buttonText": "Columns",
                "buttonText": "Filter Columns",
                //"overlayFade": 0,
                //"align": "left",
            },
            "language": {
                "lengthMenu": '_MENU_ entries per page',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "ajax": {
                url: "<?php echo base_url() . 'admin/Users/user_list';?>",
                type: "POST",

            },
            "columnDefs": [
                {
                    "orderable": false,

                }
            ]
        });

        $(document).on('click', '.deleteUser', function(){
            var user_id = $(this).attr("id");
            console.log(user_id);
            $('#deletemodal').modal('show');
            $('#btnDelete').unbind().click(function(){
                $.ajax({
                    url:"<?php echo base_url(); ?>admin/Users/delete_user",
                    method:"POST",
                    data:{user_id:user_id},
                    success:function()
                    {
                        // alert(data);
                        $('#alertTable').html('<a id="toast-success" class="btn btn-block btn-raised btn-default-bright ink-reaction">' +
                            '<i class="md md-notifications pull-right text-success"></i>' +
                            'Record  Deleted successfully' +
                            '</a>'
                        ).fadeIn().delay(1500).fadeOut('slow');
                        dataTable.ajax.reload();
                    },
                    error: function(){
                        alert('Error deleting');
                    }
                });
            });

        });
    });
</script>