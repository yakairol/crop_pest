<section class="full-bleed" style="margin-top:20px;">
    <div class="section-body style-default-dark force-padding text-shadow">
        <!--<div class="img-backdrop" style="background-image: url('<?//= base_url() ?>public/assets/img/img16.jpg')"></div> -->
        <div class="img-backdrop" style="background-color:blue;"></div>
        <div class="overlay overlay-shade-top stick-top-left height-3"></div>
        <div class="row">
            <div class="col-md-3 col-xs-5">
                <?php $pic = $user_details['profile_pic']; ?>
                <img class="img-circle border-white border-xl img-responsive auto-width user_profile" src="<?php echo base_url('uploads/images/').$pic; ?>" alt="" />
                <h3> <?= $user_details['username']; ?>
                    <br/>
                    <small class=""> <?= $user_details['is_admin']; ?></small>
                </h3>
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>
<div class="section-body no-margin">
    <div class="row" style="margin-top:10px;">
        <!-- BEGIN LAYOUT LEFT ALIGNED -->
        <div class="col-md-9">
            <!--<div class="col-md-8">-->
            <div class="card">

                <div class="card-head">
                    <div class="tools">
                        <div class="btn-group">
                            <!--<span data-toggle="tooltip"  data-placement="top"  data-original-title="Print Patient Vaccine Details">
                               <a class="btn btn-floating-action btn-primary" style="color:black;background-color: #E8E8E8;" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a>
                            </span>-->
                            <span data-toggle="tooltip"  data-placement="top"  data-original-title="Return Back">
                                <a class="btn btn-floating-action btn-primary" style="color:black;background-color: #E8E8E8;" href="<?= base_url('admin/Users') ?>" > <i class="md md-backspace"></i></a>
                             </span>
                        </div>
                    </div>
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#first1">Personal Details</a></li>
                        <li><a href="#first2">Other Details</a></li>
                        <!--  <li><a href="#second1">Contact Details</a></li>-->
                    </ul>
                </div><!--end .card-head -->
                <div class="card-body tab-content">
                    <div class="tab-pane active" id="first1">
                        <div class="col-xs-6">
                            <dl class="dl-horizontal">
                                <dt>User Name :</dt><dd><?= $user_details['username']; ?></dd>
                                <dt>First Name :</dt><dd><?= $user_details['firstname'];  ?></dd>
                                <dt>Last Name :</dt><dd><?= $user_details['lastname'];  ?></dd>
                            </dl>
                        </div><!--end .col -->
                        <div class="col-xs-6">
                            <dl class="dl-horizontal">
                                <dt>Email Address :</dt><dd><?= $user_details['email'];  ?></dd>
                                <dt>Mobile Number : </dt><dd><?= $user_details['mobile_no'];  ?></dd>
                                <dt>Role :</dt><dd><?= $user_details['is_admin'];  ?></dd>
                            </dl>
                        </div><!--end .col -->
                    </div>
                    <div class="tab-pane" id="first2">
                        <div class="col-xs-6">
                            <dl class="dl-horizontal">
                                <dt>Created At : </dt><dd><?= $user_details['created_at'];  ?></dd>
                                <dt>Last IP :</dt><dd><?= $user_details['last_ip'];  ?></dd>
                            </dl>
                        </div><!--end .col -->
                        <div class="col-xs-6">
                            <dl class="dl-horizontal">
                                <dt>Updated At :</dt><dd><?= $user_details['updated_at'];  ?></dd>
                                <dt>Status : </dt><dd><?= $user_details['status'];  ?></dd>
                            </dl>
                        </div><!--end .col -->
                    </div>
                </div>
            </div>
        </div>
        <?php
        $id = $this->uri->segment(4);
        ?>
        <div class="col-lg-offset-0 col-lg-3 col-md-3">
            <!--<div class="col-lg-offset-1 col-lg-3 col-md-4">-->
            <div class="card card-underline style-default-dark">
                <div class="card-head">
                    <header class="opacity-75"><small>User Profile</small></header>
                    <div class="tools">
                          <span data-toggle="tooltip"  data-placement="top"  data-original-title="Edit Profile">
                                <a class="btn btn-icon-toggle ink-reaction" href="<?= base_url('admin/Users/updateUserProfile'.'/'.$id); ?>" ><i class="md md-edit"></i></a>
                          </span>
                    </div><!--end .tools -->
                </div><!--end .card-head -->
                <div class="card-body no-padding">
                    <ul class="list">
                        <li class="tile">
                            <a class="tile-content ink-reaction">
                                <div class="tile-icon">
                                    <i class="md md-receipt"></i></i>
                                </div>
                                <div class="tile-text"><b>User Id</b><small>
                                        <?= $user_details['id'];  ?>
                                    </small>
                                </div>
                            </a>
                        </li>
                        <li class="divider-inset"></li>
                        <li class="tile">
                            <a class="tile-content ink-reaction">
                                <div class="tile-icon">
                                    <i class="fa fa-hospital-o"></i>
                                </div>
                                <div class="tile-text">Mobile Number <small><?= $user_details['mobile_no'];  ?></small>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div><!--end .col -->

    </div>
</div>