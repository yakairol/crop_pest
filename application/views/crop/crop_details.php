<div class="section-body no-margin">
    <div class="row" style="margin-top:10px;">
        <!-- BEGIN LAYOUT LEFT ALIGNED -->
        <div class="col-md-12 col-lg-12 col-sm-12 ">
            <!--<div class="col-md-8">-->
            <div class="card">

                <div class="card-head">
                    <div class="tools">
                        <div class="btn-group">
                            <!--<span data-toggle="tooltip"  data-placement="top"  data-original-title="Print Patient Vaccine Details">
                               <a class="btn btn-floating-action btn-primary" style="color:black;background-color: #E8E8E8;" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a>
                            </span>-->
                            <span data-toggle="tooltip"  data-placement="top"  data-original-title="Return Back">
                                <a class="btn btn-floating-action btn-primary" style="color:black;background-color: #E8E8E8;" href="<?= base_url('crop/Crop_pest_view') ?>" > <i class="md md-backspace"></i></a>
                             </span>
                        </div>
                    </div>
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#first1"> CROP PEST WISE</a></li>
                        <li><a href="#first2"> Taxonomy & Images</a></li>
                        <li><a href="#first3"> DISTRIBUTION</a></li>
                        <li><a href="#first4"> LIFE CYCLE</a></li>
                        <li><a href="#first5"> NATURAL ENEMIES</a></li>
                        <!--  <li><a href="#second1">Contact Details</a></li>-->
                    </ul>
                </div><!--end .card-head -->
                <div class="card-body tab-content">
                    <div class="tab-pane active" id="first1">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>CROP PEST DETAILS</header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>CATEGORY :</dt><dd><?= $taxo4['pest_det_category']; ?></dd>
                                                    <dt>CROP :</dt><dd><?= $taxo4['pest_det_crop'];  ?></dd>
                                                    <dt>PEST :</dt><dd><?= $taxo4['pest_det_pest'];  ?></dd>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i
                                                        class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>ENTER PAPER REFERENCE</header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12" style="text-align:center;">
                                               <!-- <h5>Text or URL Reference </h5>-->
                                            </div><!--end .col -->
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>CATEGORY :</dt><dd><?= $taxo5['paper_reference_category']; ?></dd>
                                                    <dt>CROP :</dt><dd><?= $taxo5['paper_reference_crop'];  ?></dd>
                                                    <dt>PEST :</dt><dd><?= $taxo5['paper_reference_pest'];  ?></dd>
                                                    <dt>PEST STATUS :</dt><dd><?= $taxo5['paper_reference_pest_status'];  ?></dd>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->


                            </div><!--end .col -->
                        </div><!--end .row -->
                    </div>
                    <div class="tab-pane" id="first2">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>Taxonomy & Images </header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <dl class="dl-horizontal">
                                                    <dt>Kingdom :</dt><dd><?= $taxo['taxonomy_kingdom']; ?></dd>
                                                    <dt>Phylum :</dt><dd><?= $taxo['taxonomy_phylum'];  ?></dd>
                                                    <dt>Class :</dt><dd><?= $taxo['taxonomy_class'];  ?></dd>
                                                    <dt>Order :</dt><dd><?= $taxo['taxonomy_order'];  ?></dd>
                                                    <dt>Family :</dt><dd><?= $taxo['taxonomy_family'];  ?></dd>
                                                    <dt>Genus :</dt><dd><?= $taxo['taxonomy_genus'];  ?></dd>
                                                    <dt>Species :</dt><dd><?= $taxo['taxonomy_species'];  ?></dd>
                                                    <dt>Sub-species :</dt><dd><?= $taxo['taxonomy_sub_species'];  ?></dd>
                                                    <dt>Variety :</dt><dd><?= $taxo['taxonomy_variety'];  ?></dd>
                                                </dl>
                                            </div><!--end .col -->
                                            <div class="col-xs-6">
                                                <dl class="dl-horizontal">
                                                    <dt>Scientific Name :</dt><dd><?= $taxo['taxonomy_scientific_name']; ?></dd>
                                                    <dt>Accepted Name :</dt><dd><?= $taxo['taxonomy_accepted_name'];  ?></dd>
                                                    <dt>Reference :</dt><dd><?= $taxo['taxonomy_reference'];  ?></dd>
                                                    <dt>Name Published Year :</dt><dd><?= $taxo['taxonomy_published_year'];  ?></dd>
                                                    <dt>Name Published in :</dt><dd><?= $taxo['taxonomy_published_in'];  ?></dd>
                                                    <dt>Nomenclature Code :</dt><dd><?= $taxo['taxonomy_nomen_code'];  ?></dd>
                                                    <dt>Published :</dt><dd  ><?= $taxo['taxonomy_published'];  ?></dd>
                                                    <dt>Common Name:<br>
                                                        /Vernacular Name
                                                    </dt>
                                                    <dd>
                                                        <?php $i = 1;
                                                        foreach($taxo2 as $row) { ?>
                                                        <?= "$i".') '.$row->common_ver_name; ?>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            <?php $i++; } ?>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-12">
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="table-responsive" >
                                                    <table id="datatable11" class="table table-striped table-hover" style="border:1px solid grey">
                                                        <thead>
                                                        <tr>
                                                            <th>SYNONYMS</th>
                                                            <th>Reference</th>
                                                            <th>Year</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach($taxo3 as $row) { ?>
                                                        <tr>
                                                            <td><?= $row->taxonomy_syn_name; ?></td>
                                                            <td><?= $row->taxonomy_syn_ref; ?></td>
                                                            <td><?= $row->taxonomy_syn_year; ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div><!--end .table-responsive -->
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12">
                                            </div>
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                        </div><!--end .row -->

                    </div>
                    <div class="tab-pane" id="first3">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>DISTRIBUTION</header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>DISTRIBUTION :</dt><dd><?= $taxo6['distribution_world_api'];  ?></dd>
                                                    <dt> </dt><dd><?= $taxo6['distribution_india_api'];  ?></dd>
                                                    <dt>DISTRIBUTION :</dt><dd><?= $taxo6['distribution_district'];  ?></dd>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header> GENERAL IMAGES </header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                GENERAL IMAGE UPLOADED
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i
                                                        class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>INTRODUCTION & NATURAL HISTORY</header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>INTRODUCTION :</dt><dd><?= $taxo7['natural_history_intro'];  ?>&nbsp;&nbsp;<?= $taxo7['natural_history_intro_ref'];  ?></dd>
                                                    <dt>STAGE OF PEST ATTACK :</dt><dd><?= $taxo7['natural_history_stage'];  ?>&nbsp;&nbsp;<?= $taxo7['natural_history_stage_ref'];  ?></dd>
                                                    <dt>SYMPTOMS OF PEST : <br> ATTACK</dt><dd><?= $taxo7['natural_history_affected_part'];  ?>&nbsp;&nbsp;<?= $taxo7['natural_history_affected_part_ref']; ?></dd>
                                                    <dd><?= $taxo7['natural_history_damage_symtoms'];  ?></dd>
                                                    <dt>ALTERNATE HOST :</dt><dd><?= $taxo7['natural_history_position_site'];  ?>&nbsp;&nbsp;<?= $taxo7['natural_history_position_site_ref'];  ?></dd>
                                                    <dt></dt><dd><?= $taxo7['natural_history_pupation_site'];  ?>&nbsp;&nbsp;<?= $taxo7['natural_history_pupation_site_ref'];  ?></dd>
                                                    <dt></dt><dd><?= $taxo7['natural_history_diapause'];  ?>&nbsp;&nbsp;<?= $taxo7['natural_history_diapause_ref'];  ?></dd>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                SYMPTOM MAGES
                                            </div>
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->


                            </div><!--end .col -->
                        </div><!--end .row -->
                    </div>
                    <div class="tab-pane" id="first4">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>LIFE CYCLE </header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>LIFE CYCLE IN FIELD :<br>CONDITION </dt><dd><?= $taxo8['life_cycle_field_condition'];  ?>&nbsp;&nbsp;<?= $taxo8['life_cycle_field_condition_ref'];  ?></dd>
                                                    <dt>OVIPOSITION SITE :</dt><dd><?= $taxo8['life_cycle_oviposition_site'];  ?>&nbsp;&nbsp;<?= $taxo8['life_cycle_oviposition_site_ref'];  ?></dd>
                                                    <dt>DIAPAUSE :</dt><dd><?= $taxo8['life_cycle_diapause'];  ?>&nbsp;&nbsp;<?= $taxo8['life_cycle_diapause_ref'];  ?></dd>
                                                    <dt>PUPATION SITE :</dt><dd><?= $taxo8['life_cycle_pupation_site'];  ?>&nbsp;&nbsp;<?= $taxo8['life_cycle_pupation_site_ref'];  ?></dd>
                                                    <dt>LIFE CYCLE IN LAB CONDITION :</dt><dd><?= $taxo8['life_cycle_lab_condition'];  ?>&nbsp;&nbsp;<?= $taxo8['life_cycle_lab_condition_ref'];  ?></dd>
                                                    <dt>BIO-ECOLOGY :</dt><dd><?= $taxo8['life_cycle_bio_ecology'];  ?>&nbsp;&nbsp;<?= $taxo8['life_cycle_bio_ecology_ref'];  ?></dd>
                                                    <dt>DISPERSAL MOVEMENT :</dt><dd><?= $taxo8['life_cycle_dispersal'];  ?>&nbsp;&nbsp;<?= $taxo8['life_cycle_dispersal_ref'];  ?></dd>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                        <div class="row">
                                            LIFE CYCLE IMAGES &nbsp;&nbsp;  REF (TEXT/URL)<?//= $user_details['username']; ?>
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i
                                                        class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>PREVENTION & MANAGEMENT</header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>ETL :</dt><dd><?= $taxo9['prevention_tel'];  ?>&nbsp;&nbsp;<?= $taxo9['prevention_etl_ref'];  ?></dd>
                                                    <dt>CULTURAL CONTROL :</dt><dd><?= $taxo9['prevention_cultural'];  ?>&nbsp;&nbsp;<?= $taxo9['prevention_cultural_ref'];  ?></dd>
                                                    <dt>MECHANICAL CONTROL :</dt><dd><?= $taxo9['prevention_mechanical'];  ?>&nbsp;&nbsp;<?= $taxo9['prevention_mechanical_ref'];  ?></dd>
                                                    <dt>PHYSICAL CONTROL :</dt><dd><?= $taxo9['prevention_physical'];  ?>&nbsp;&nbsp;<?= $taxo9['prevention_physical_ref'];  ?></dd>
                                                    <dt>BIOLOGICAL CONTROL :</dt><dd><?= $taxo9['prevention_biological'];  ?>&nbsp;&nbsp;<?= $taxo9['prevention_biological_ref'];  ?></dd>
                                                    <dt>CHEMICAL CONTROL :</dt><dd><?= $taxo9['prevention_insecticide'];  ?>&nbsp;&nbsp;<?= $taxo9['prevention_insecticide_ref'];  ?></dd>
                                                    <dt><?= $taxo9['prevention_trade_name'];  ?></dt><dd><?= $taxo9['prevention_company'];  ?></dd>
                                                    <dt><?= $taxo9['prevention_active_ingredient'];  ?></dt><dd><?= $taxo9['prevention_dosage'];  ?></dd>
                                                    <dt>RESISTANT VARIETY :</dt><dd><?= $taxo9['prevention_resistant_variety'];  ?>&nbsp;&nbsp;<?= $taxo9['prevention_resistant_variety_ref'];  ?></dd>
                                                    <dt>TOLERANT VARIETY :</dt><dd><?= $taxo9['prevention_tolerant_name'];  ?>&nbsp;&nbsp;<?= $taxo9['prevention_tolerant_name_ref'];  ?></dd>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->


                            </div><!--end .col -->
                        </div><!--end .row -->
                    </div>
                    <div class="tab-pane" id="first5">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <center>
                                            <header>NATURAL ENEMIES</header>
                                        </center>
                                    </div><!--end .card-head -->
                                </div><!--end .card -->
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>PARASITE</header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>PARASITE :</dt>
                                                    <?php foreach($taxo10 as $row) {  ?>
                                                        <dd><?= $row->parasite_stage; ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row->parasite_ref ?></dd>
                                                    <?php } ?>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header>PREDATORS</header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>PREDATOR :</dt>
                                                    <?php foreach($taxo11 as $row) {  ?>
                                                        <dd><?= $row->pradators_stage; ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row->pradators_ref ?></dd>
                                                    <?php } ?>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <center>
                                            <header> YIELD LOSS & ECONOMICS </header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <dt>ETL :</dt>
                                                    <?php foreach($taxo12 as $row) {  ?>
                                                        <dd><?= $row->yield_loss_etl; ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row->yield_loss_ref ?></dd>
                                                    <?php } ?>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                                <div class="card card-bordered style-primary">
                                    <div class="card-head">
                                        <center>
                                            <header> ADDITIONAL INFORMATION </header>
                                        </center>
                                    </div><!--end .card-head -->
                                    <div class="card-body style-default-bright">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <dl class="dl-horizontal">
                                                    <?php foreach($taxo13 as $row) {  ?>
                                                        <dd><?= $row->add_info_name; ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= $row->add_info_ref ?></dd>
                                                    <?php } ?>
                                                </dl>
                                            </div><!--end .col -->
                                        </div>
                                    </div><!--end .card-body -->
                                </div><!--end .card -->
                            </div><!--end .col -->
                        </div><!--end .row -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable11').DataTable({
            "dom": 't',
            //"dom": 'tp',
            //dom: '1BCfrtip',
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "order": [],
            //"dom": 'lCfrtip', //  l = entries,C = column (COVIES) ,f = search bar,  p = pagination in abpve, tp = pagination below,i = shpwing 1 to 6
            "language": {
                "lengthMenu": '_MENU_ entries per page',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "columnDefs": [
                {
                    "orderable": false,

                }
            ]
        });
        $('#datatable12').DataTable({
            "dom": 't',
            //"dom": 'tp',
            //dom: '1BCfrtip',
            "processing": true,
            "serverSide": false,
            "autoWidth": false,
            "order": [],
            //"dom": 'lCfrtip', //  l = entries,C = column (COVIES) ,f = search bar,  p = pagination in abpve, tp = pagination below,i = shpwing 1 to 6
            "language": {
                "lengthMenu": '_MENU_ entries per page',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "columnDefs": [
                {
                    "orderable": false,

                }
            ]
        });
    });

</script>