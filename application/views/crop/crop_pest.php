<link href='<?= base_url() ?>resources/dropzone.css' type='text/css' rel='stylesheet'>
<script src='<?= base_url() ?>resources/dropzone.js' type='text/javascript'></script>
<script>
    // Add restrictions
    Dropzone.options.fileupload = {
        acceptedFiles: 'image/*,.pdf',
        maxFilesize: 1, // MB
        dictDefaultMessage: 'Drop image here (or click) to capture/upload',
        parallelUploads: 10,
        maxFiles:5,
        autoProcessQueue: false,
        addRemoveLinks: true,
        init: function () {

            var myDropzone = this;

            // Update selector to match your button
            $("#button").click(function (e) {
                e.preventDefault();
                myDropzone.processQueue();
            });
            this.on('sending', function(file, xhr, formData) {
                // Append all form inputs to the formData Dropzone will POST
                var data = $('#fileupload').serializeArray();
                $.each(data, function(key, el) {
                    formData.append(el.name, el.value);
                });
            });
        }
    };
    Dropzone.options.fileupload1 = {
        acceptedFiles: 'image/*,.pdf',
        maxFilesize: 1, // MB
        dictDefaultMessage: 'Drop herarium here (or click) to capture/upload',
        parallelUploads: 10,
        maxFiles:5,
        autoProcessQueue: false,
        addRemoveLinks: true,
        init: function () {

            var myDropzone = this;

            // Update selector to match your button
            $("#button").click(function (e) {
                e.preventDefault();
                myDropzone.processQueue();
            });
            this.on('sending', function(file, xhr, formData) {
                // Append all form inputs to the formData Dropzone will POST
                var data = $('#fileupload1').serializeArray();
                $.each(data, function(key, el) {
                    formData.append(el.name, el.value);
                });
            });
        }
    };

    Dropzone.options.fileupload2 = {
        acceptedFiles: 'image/*,.pdf',
        maxFilesize: 1, // MB
        dictDefaultMessage: 'Drop painting here (or click) to capture/upload',
        parallelUploads: 10,
        maxFiles:5,
        autoProcessQueue: false,
        addRemoveLinks: true,
        init: function () {

            var myDropzone = this;

            // Update selector to match your button
            $("#button").click(function (e) {
                e.preventDefault();
                myDropzone.processQueue();
            });
            this.on('sending', function(file, xhr, formData) {
                // Append all form inputs to the formData Dropzone will POST
                var data = $('#fileupload2').serializeArray();
                $.each(data, function(key, el) {
                    formData.append(el.name, el.value);
                });
            });
        }
    };
</script>
<div class="section-body">
    <div class="row">
        <div class="col-lg-offset-0 col-md-12 col-lg-12 col-sm-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                        <?php echo form_open_multipart((''), 'class="form floating-label form-validation form-validate floating-label" id="myform"  role="form" novalidate="novalidate"');  ?>

                        <?php

                        // Output: 5fa7f46962f25;
                        $unique=uniqid();

                        ?>

                        <input hidden type="text" name="uni_id" value="<?= $unique?>">
                        <div class="form-wizard-nav">
                                <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                                <ul class="nav nav-justified">
                                    <li><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">Crop Pest</span></a>
                                    </li>
                                    <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Taxonomy </span></a>
                                    </li>
                                    <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Distribution</span></a>
                                    </li>
                                    <li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">Life Cycle</span></a>
                                    </li>
                                    <li><a href="#step5" data-toggle="tab"><span class="step">5</span> <span class="title">Natural Enemies</span></a>
                                    </li>
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="step1">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">
                                                    <center>
                                                        <header>CROP PEST DETAILS</header>
                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">
                                                    <div class="form-group">
                                                        <select id="select1" name="pest_det_category" class="form-control">
                                                            <option value="">&nbsp;</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                        </select>
                                                        <label for="select1">Category</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <select id="select1" name="pest_det_crop" class="form-control">
                                                            <option value="">&nbsp;</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                        </select>
                                                        <label for="select1">Crop</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <select id="select1" name="pest_det_pest" class="form-control">
                                                            <option value="">&nbsp;</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                        </select>
                                                        <label for="select1">Pest</label>
                                                    </div>

                                                </div><!--end .card-body -->
                                            </div><!--end .card -->
                                        </div><!--end .col -->


                                        <div class="col-md-6">

                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">
                                                    <div class="tools">
                                                        <div class="btn-group">
                                                            <a class="btn btn-icon-toggle btn-collapse"><i
                                                                        class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div>
                                                    <center>
                                                        <header>ENTER PAPER REFERENCE</header>
                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">
                                                    <div class="form-group">
                                                        <input type="text" name="kingdom" id="kingdom"
                                                               class="form-control" >
                                                        <label for="kingdom" class="control-label">Add Reference</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <select id="select1" name="paper_reference_category" class="form-control">
                                                            <option value="">&nbsp;</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                        </select>
                                                        <label for="select1">Category</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <select id="select1" name="paper_reference_crop" class="form-control">
                                                            <option value="">&nbsp;</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                        </select>
                                                        <label for="select1">Crop</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <select id="select1" name="paper_reference_pest" class="form-control">
                                                            <option value="">&nbsp;</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                        </select>
                                                        <label for="select1">Pest</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <select id="select1" name="paper_reference_pest_status" class="form-control">
                                                            <option value="">&nbsp;</option>
                                                            <option value="30">30</option>
                                                            <option value="40">40</option>
                                                            <option value="50">50</option>
                                                            <option value="60">60</option>
                                                            <option value="70">70</option>
                                                        </select>
                                                        <label for="select1">Pest Status</label>
                                                    </div>

                                                </div><!--end .card-body -->
                                            </div><!--end .card -->


                                        </div><!--end .col -->
                                    </div><!--end .row -->



                                </div><!--end #step1 -->



                                <div class="tab-pane" id="step2">
                                    <br/><br/>
                                    <div class="card card-bordered style-primary">
                                        <div class="card-head">
                                            <center>
                                                <header>TAXONOMY & IMAGES
                                                </header>
                                            </center>
                                        </div><!--end .card-head -->
                                        <div class="card-body style-default-bright">
                                            <div class="table-responsive">
                                                <table class="table table-bordered no-margin">
                                                    <thead>
                                                    <tr>
                                                        <th>SYNONYMS</th>
                                                        <th>REFERENCE</th>
                                                        <th>YEAR</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><input type="text" class="form-control syn_one" name="taxonomy_syn_name_one" id="taxonomy_syn_name_one"></td>
                                                        <td><input type="text" class="form-control refer_one" name="taxonomy_syn_ref_one" id="taxonomy_syn_ref_one"></td>
                                                        <td><input type="text" class="form-control year_one" name="taxonomy_syn_year_one" id="taxonomy_syn_year_one"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" class="form-control syn_two" name="taxonomy_syn_name_two" id="taxonomy_syn_name_two"></td>
                                                        <td><input type="text" class="form-control refer_two" name="taxonomy_syn_ref_two" id="taxonomy_syn_ref_two"></td>
                                                        <td><input type="text" class="form-control year_two" name="taxonomy_syn_year_two" id="taxonomy_syn_year_two"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" class="form-control syn_three" name="taxonomy_syn_name_three" id="taxonomy_syn_three"></td>
                                                        <td><input type="text" class="form-control refer_three" name="taxonomy_syn_ref_three" id="taxonomy_syn_ref_three"></td>
                                                        <td><input type="text" class="form-control year_three" name="taxonomy_syn_year_three" id="taxonomy_syn_year_three"></td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_kingdom" id="syn_family"
                                                               class="form-control" >
                                                        <label for="syn_family" class="control-label">Kingdom</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_phylum" id="syn_genus"
                                                               class="form-control" >
                                                        <label for="syn_genus" class="control-label">Phylum</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_class" id="syn_species"
                                                               class="form-control" >
                                                        <label for="syn_species" class="control-label">Class</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_order" id="syn_subspecies"
                                                               class="form-control" >
                                                        <label for="syn_subspecies" class="control-label">Order</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_family" id="syn_variety"
                                                               class="form-control" >
                                                        <label for="syn_variety" class="control-label">Family</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_genus" id="syn_genus"
                                                               class="form-control" >
                                                        <label for="syn_genus" class="control-label">Genus</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_species" id="syn_species"
                                                               class="form-control" >
                                                        <label for="syn_species" class="control-label">Species</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_sub_species" id="syn_subspecies"
                                                               class="form-control" >
                                                        <label for="syn_subspecies" class="control-label">Sub-species</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_variety" id="syn_variety"
                                                               class="form-control" >
                                                        <label for="syn_variety" class="control-label">Variety</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="form-group">
                                                        <input required type="text" name="taxonomy_scientific_name" id="taxonomy_scientific_name"
                                                               class="form-control" >
                                                        <label for="syn_familyrefer" class="control-label">Scientific Name <span style="color: red">*</span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input required type="text" name="taxonomy_accepted_name" id="taxonomy_accepted_name"
                                                               class="form-control" >
                                                        <label for="syn_genusrefer" class="control-label">Accepted Name<span style="color: red">*</span></label>
                                                    </div>
                                                    <div  class="form-group">
                                                        <input required type="text" name="taxonomy_reference" id="taxonomy_reference"
                                                               class="form-control" >
                                                        <label for="syn_speciesrefer" class="control-label">Reference<span style="color: red">*</span></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_published_year" id="datepicker"
                                                               class="form-control" >
                                                        <label for="syn_subspeciesrefer" class="control-label">Name Published Year
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_published_in" id="syn_varietyrefer"
                                                               class="form-control" >
                                                        <label for="syn_varietyrefer" class="control-label">Name Published in
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_nomen_code" id="syn_genusrefer"
                                                               class="form-control" >
                                                        <label for="syn_genusrefer" class="control-label">Nomenclature Code
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="taxonomy_published" id="syn_speciesrefer"
                                                               class="form-control" >
                                                        <label for="syn_speciesrefer" class="control-label">Published</label>
                                                    </div>
                                                    <h4>Common Name/ Vernacular Name</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="taxonomy_common_name" id="lol" class="form-control" >
                                                                <label for="lol" class="control-label">Common Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="taxonomy_vernacular_name" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Vernacular Name</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div><!--end .card-body -->
                                    </div><!--end .card -->

                                </div><!--end #step2 -->




                                <div class="tab-pane" id="step3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">

                                                    <center>
                                                        <header>DISTRIBTUION</header>
                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">
                                                    <h4>Distribution</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="distribution_world_api" id="distribution_world_api" class="form-control" placeholder="" >
                                                                <label for="distribution_world_api" class="control-label">World Distribution</label>
                                                            </div>
                                                            <input type="text" name="world_dis_lat" id="world_dis_lat" class="form-control hidden" placeholder="" >
                                                            <input type="text" name="world_dis_lng" id="world_dis_lng" class="form-control hidden" placeholder="" >
                                                            <input type="text" name="world_dis_city" id="world_dis_city" class="form-control hidden" placeholder="" >
                                                            <input type="text" name="world_dis_state" id="world_dis_state" class="form-control hidden" placeholder="" >
                                                            <input type="text" name="world_dis_country" id="world_dis_country" class="form-control hidden" placeholder="" >
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="distribution_india_api" id="distribution_india_api" class="form-control" placeholder="" >
                                                                <label for="distribution_india_api" class="control-label">India Distribution</label>
                                                            </div>
                                                            <input type="text" name="ind_dis_lat" id="ind_dis_lat" class="form-control hidden" placeholder="" >
                                                            <input type="text" name="ind_dis_lng" id="ind_dis_lng" class="form-control hidden" placeholder="" >
                                                            <input type="text" name="ind_dis_city" id="ind_dis_city" class="form-control hidden" placeholder="" >
                                                            <input type="text" name="ind_dis_state" id="ind_dis_state" class="form-control hidden" placeholder="" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="distribution_district" id="kingdom"
                                                               class="form-control" >
                                                        <label for="kingdom" class="control-label">Distribution Districts</label>
                                                    </div>
                                                </div><!--end .card-body -->
                                            </div><!--end .card -->
                                        </div><!--end .col -->

                                        <div class="col-md-6">

                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">

                                                    <center>
                                                        <header>INTRODUCTION & NATURAL HISTORY</header>
                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">
                                                    <h4>INTRODUCTION</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_intro" id="world_dis" class="form-control" >
                                                                <label for="world_dis" class="control-label">Introduction Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_intro_ref" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Introduction Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>STAGE OF PEST ATTACK
                                                    </h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_stage" id="world_dis" class="form-control" >
                                                                <label for="world_dis" class="control-label">Stage Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_stage_ref" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Stage Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>SYMPTOMS OF PEST ATTACK</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select id="natural_history_affected_part" name="select1" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="30">30</option>
                                                                    <option value="40">40</option>
                                                                    <option value="50">50</option>
                                                                    <option value="60">60</option>
                                                                    <option value="70">70</option>
                                                                </select>
                                                                <label for="select1">Plant Part Affected</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_affected_part_ref" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Plant Part Affected Ref</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <select id="select1" name="natural_history_damage_symtoms" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="30">30</option>
                                                                    <option value="40">40</option>
                                                                    <option value="50">50</option>
                                                                    <option value="60">60</option>
                                                                    <option value="70">70</option>
                                                                </select>
                                                                <label for="select1">Damage Symtom</label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <h4>ALTERNATE HOST</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_position_site" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Oviposition Site</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_position_site_ref" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Oviposition Site Ref</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_pupation_site" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Pupation Site</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="ind_disnatural_history_pupation_site_ref" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Pupation Site Ref</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_diapause" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Diapause/No. of generations</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="natural_history_diapause_ref" id="ind_dis" class="form-control" >
                                                                <label for="ind_dis" class="control-label">Diapause Ref</label>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div><!--end .card-body -->
                                            </div><!--end .card -->


                                        </div><!--end .col -->



                                        <?php echo form_close( ); ?>

                                        <div class="col-md-6">
                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">

                                                    <center>
                                                        <header>GENERAL IMAGES</header>
                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">
                                                    <form  action="<?php echo base_url("crop/crop_pest_image/general_image/"); ?>" class="dropzone "  id="fileupload">
                                                        <input hidden type="text" name="uni_id" value="<?= $unique?>">
                                                        <div class="dz-message">
                                                            <h3><i class="md md-file-upload "></i></h3>
                                                            <em>(GENERAL IMAGES UPLOAD.)</em>

                                                        </div>

                                                    </form>

                                                </div><!--end .card-body -->
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">

                                                    <center>
                                                        <header>SYMPTOM IMAGES</header>
                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">
                                                    <form  action="<?php echo base_url("crop/crop_pest_image/symtom_image/"); ?>" class="dropzone "  id="fileupload1">
                                                        <input hidden type="text" name="uni_id" value="<?= $unique?>">
                                                        <div class="dz-message">
                                                            <h3><i class="md md-file-upload "></i></h3>
                                                            <em>(Symtom images upload.)</em>

                                                        </div>

                                                    </form>

                                                </div><!--end .card-body -->
                                            </div>

                                        </div><!--end .col -->




                                    </div><!--end .row -->

                                </div><!--end #step3 -->


                                <div class="tab-pane" id="step4">
                                    <?php echo form_open_multipart((''), 'class="form floating-label form-validation form-validate floating-label" id="myform2"  role="form" novalidate="novalidate"');  ?>

                                    <?php

                                    // Output: 5fa7f46962f25;
                                    $unique=uniqid();

                                    ?>
                                    <input hidden type="text" name="uni_id" value="<?= $unique?>">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">
                                                    <div class="tools">
                                                        <div class="btn-group">
                                                            <a class="btn btn-icon-toggle btn-collapse"><i
                                                                        class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div>
                                                    <center>
                                                        <header>LIFE CYCLE</header>

                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">


                                                    <h4>LIFE CYCLE IN FIELD CONDITION</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <select id="select1" name="life_cycle_field_condition" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="30">30</option>
                                                                    <option value="40">40</option>
                                                                    <option value="50">50</option>
                                                                    <option value="60">60</option>
                                                                    <option value="70">70</option>
                                                                </select>
                                                                <label for="select1">Select Field Condition</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_field_condition_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Field Condition Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>OVIPOSITION SITE</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_oviposition_site" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Oviposition Site Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_oviposition_site_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Oviposition Site Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>DIAPAUSE</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_diapause" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Diapause Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_diapause_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Diapause Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>PUPATION SITE</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_pupation_site" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Pupation Site Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_pupation_site_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Pupation Site Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>LIFE CYCLE IN LAB CONDITION</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <select id="select1" name="life_cycle_lab_condition" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="30">30</option>
                                                                    <option value="40">40</option>
                                                                    <option value="50">50</option>
                                                                    <option value="60">60</option>
                                                                    <option value="70">70</option>
                                                                </select>
                                                                <label for="select1">Select Lab Condition</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_lab_condition_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Lab Condition Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>BIO-ECOLOGY</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_bio_ecology" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Bio-Ecology Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_bio_ecology_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Bio-Ecology Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>DISPERSAL MOVEMENT</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_dispersal" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Dispersal Movement Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="life_cycle_dispersal_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Dispersal Movement Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--end .card-body -->
                                            </div><!--end .card -->


                                        </div><!--end .col -->
                                        <div class="col-md-6">
                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">
                                                    <div class="tools">
                                                        <div class="btn-group">
                                                            <a class="btn btn-icon-toggle btn-collapse"><i
                                                                        class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div>
                                                    <center>
                                                        <header>PREVENTION & MANAGEMENT</header>

                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">
                                                    <h4>ETL</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_tel" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">ETL Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_etl_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">ETL Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>CULTURAL CONTROL</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_cultural" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Cultural Control Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_cultural_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Cultural Control Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>MECHANICAL CONTROL</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_mechanical" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Mechanical Control Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_mechanical_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Mechanical Control Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>PHYSICAL CONTROL</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_physical" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Physical Control Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_physical_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Physical Control Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>BIOLOGICAL CONTROL</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_biological" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Biological Control Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_biological_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Biological Control Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>CHEMICAL CONTROL</h4>
                                                    <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <select id="select1" name="prevention_insecticide" class="form-control">
                                                                        <option value="">&nbsp;</option>
                                                                        <option value="30">30</option>
                                                                        <option value="40">40</option>
                                                                        <option value="50">50</option>
                                                                        <option value="60">60</option>
                                                                        <option value="70">70</option>
                                                                    </select>
                                                                    <label for="select1">Insecticide Name</label>
                                                                </div>
                                                            </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_insecticide_ref" id="locn" class="form-control" >
                                                                <label for="locn" class="control-label">Insecticide Ref</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <select id="prevention_trade_name" name="prevention_trade_name" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="30">30</option>
                                                                    <option value="40">40</option>
                                                                    <option value="50">50</option>
                                                                    <option value="60">60</option>
                                                                    <option value="70">70</option>
                                                                </select>
                                                                <label for="select1">Trade Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <select id="prevention_company" name="prevention_company" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="30">30</option>
                                                                    <option value="40">40</option>
                                                                    <option value="50">50</option>
                                                                    <option value="60">60</option>
                                                                    <option value="70">70</option>
                                                                </select>
                                                                <label for="select1">Company Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <select id="prevention_active_ingredient" name="prevention_active_ingredient" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="30">30</option>
                                                                    <option value="40">40</option>
                                                                    <option value="50">50</option>
                                                                    <option value="60">60</option>
                                                                    <option value="70">70</option>
                                                                </select>
                                                                <label for="select1">Active Ingredient</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <select id="prevention_dosage" name="prevention_dosage" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    <option value="30">30</option>
                                                                    <option value="40">40</option>
                                                                    <option value="50">50</option>
                                                                    <option value="60">60</option>
                                                                    <option value="70">70</option>
                                                                </select>
                                                                <label for="select1">Dosage</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4>RESISTANT VARIETY</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_resistant_variety" id="prevention_resistant_variety" class="form-control" >
                                                                <label for="locn" class="control-label">Resistant Variety Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_resistant_variety_ref" id="prevention_resistant_variety_ref" class="form-control" >
                                                                <label for="locn" class="control-label">Resistant Variety Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>TOLERANT VARIETY</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_tolerant_name" id="prevention_tolerant_name" class="form-control" >
                                                                <label for="locn" class="control-label">Tolerant Variety Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="prevention_tolerant_name_ref" id="prevention_tolerant_name_ref" class="form-control" >
                                                                <label for="locn" class="control-label">Tolerant Variety Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div><!--end .card-body -->
                                            </div><!--end .card -->
                                        </div><!--end .col -->
                                        <?php echo form_close( ); ?>

                                        <div class="col-md-6">
                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">

                                                    <center>
                                                        <header>LIFE CYCLE IMAGES</header>
                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">

                                                    <form  action="<?php echo base_url("crop/crop_pest_image/lifecycle_image/"); ?>" class="dropzone "  id="fileupload2">
                                                        <input hidden type="text" name="uni_id" value="<?= $unique?>">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="dz-message">
                                                                    <h3><i class="md md-file-upload "></i></h3>
                                                                    <em>(LIFE CYCLE IMAGES UPLOAD.)</em>

                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <input type="text" name="lifecycle_imgref" id="lifecycle_imgref" class="form-control" >
                                                                    <label for="locn" class="control-label">Ref</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div><!--end .card-body -->
                                            </div>

                                        </div>

                                    </div><!--end .row -->

                                </div><!--end #step4 -->


                                <!--start #step5 -->
                                <div class="tab-pane" id="step5">
                                    <?php echo form_open_multipart((''), 'class="form floating-label form-validation form-validate floating-label" id="myform3"  role="form" novalidate="novalidate"');  ?>

                                    <?php

                                    // Output: 5fa7f46962f25;
                                    $unique=uniqid();

                                    ?>
                                    <input hidden type="text" name="uni_id" value="<?= $unique?>">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card card-bordered style-primary">
                                                <div class="card-head">
                                                    <div class="tools">
                                                        <div class="btn-group">
                                                            <a class="btn btn-icon-toggle btn-collapse"><i
                                                                        class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div>
                                                    <center>
                                                        <header>NATURAL ENEMIES</header>
                                                    </center>
                                                </div><!--end .card-head -->
                                                <div class="card-body style-default-bright">
                                                    <h4>Parsite</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="parasite_name" id="parasite_name" class="form-control" >
                                                                <label for="pri_dataset" class="control-label">Parasite Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="parasite_stage" id="parasite_stage" class="form-control" >
                                                                <label for="sec_dataset" class="control-label">Parasite Stage</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="parasite_ref" id="parasite_ref" class="form-control" >
                                                                <label for="crowd_dataset" class="control-label">Parasite Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4>PREDATORS</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="pradators_name" id="pradators_name" class="form-control" >
                                                                <label for="pri_dataset" class="control-label">Predator Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="pradators_stage" id="pradators_stage" class="form-control" >
                                                                <label for="sec_dataset" class="control-label">Predator Stage</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="pradators_ref" id="pradators_ref" class="form-control" >
                                                                <label for="crowd_dataset" class="control-label">Predator Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>YIELD LOSS & ECONOMICS</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="yield_loss_etl" id="yield_loss_etl" class="form-control" >
                                                                <label for="pri_dataset" class="control-label">ETL Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="yield_loss_name" id="yield_loss_name" class="form-control" >
                                                                <label for="sec_dataset" class="control-label">Yield Loss Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <input type="text" name="yield_loss_ref" id="yield_loss_ref" class="form-control" >
                                                                <label for="crowd_dataset" class="control-label">Yield Loss Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4>ADDITINAL INFORMATIONS</h4>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="add_info_name" id="add_info_name" class="form-control" >
                                                                <label for="pri_dataset" class="control-label">Information Name</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input type="text" name="add_info_ref" id="add_info_ref" class="form-control" >
                                                                <label for="crowd_dataset" class="control-label">Information Ref</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--end .card-body -->
                                            </div><!--end .card -->
                                            <?php form_close(); ?>
                                            <input type="button" name="submit" id="button" value="Add Data" class="btn btn1 btn-primary bg-green pull-right ink-reaction">

                                        </div>
                                    </div><!--end .row -->


                                </div><!--end #step5 -->

                            </div><!--end .tab-content -->
                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">First</a></li>
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Previous</a></li>
                                <li class="next last"><a class="btn-raised" href="javascript:void(0);">Last</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Next</a></li>
                            </ul>

                    </div><!--end #rootwizard -->
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div>
    </div><!--end .col -->
</div>

<link type="text/css" rel="stylesheet"
      href="<?= base_url() ?>public/assets/css/theme-default/libs/wizard/wizard.css?1425466601"/>
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/toastr/toastr.css?1425466569" />
<link href='<?= base_url() ?>resources/dropzone.css' type='text/css' rel='stylesheet'>
<script src='<?= base_url() ?>resources/dropzone.js' type='text/javascript'></script>

<style>
    .dropzone{
        box-shadow: 0 2px 7px 0 rgba(0,0,0,0.18), 0 2px 4px 0 rgba(0,0,0,0.15);
        border: none;
    }
    body [class*=" md-"], [class^=md-] {
        font-size: 2.2em;
        margin-top: -0.2em;
        margin-bottom: -0.2em;
    }
</style>

<script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<script src="<?= base_url() ?>public/assets/js/libs/toastr/toastr.js"></script>

<script src="<?= base_url() ?>public/assets/js/libs/bootstrap-rating/bootstrap-rating-input.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCchiUzDb_DTO8rfJRQux_kVEZoCvcCqNk&amp;libraries=places"></script>


<script>

    google.maps.event.addDomListener(window, 'load', initialize);
    function initialize() {
        var input = document.getElementById('distribution_india_api');
        var options = {
            types: ["geocode"],
            componentRestrictions: {country: "IN"}
        };
        var input1 = document.getElementById('distribution_world_api');
        var options1 = {
            types: ["geocode"],
        };
        var autocomplete1 = new google.maps.places.Autocomplete(input1,options1);
        var autocomplete = new google.maps.places.Autocomplete(input,options);

        autocomplete.addListener('place_changed', function () {
            // $('#long').val(place.geometry['location'].lng());
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById('distribution_india_api').value;
            console.log(address);
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    console.log(latitude, longitude);
                    $("#ind_dis_lat").val(latitude);
                    $("#ind_dis_lng").val(longitude);
                    const latlang = {
                        lat: parseFloat(latitude),
                        lng: parseFloat(longitude),
                    };
                    function getAddressParts(obj) {
                        var address = [];
                        obj.address_components.forEach( function(el) {
                            address[el.types[0]] = el.long_name;
                        });
                        return address;
                    } //getAddressParts()
                    geocoder.geocode( { 'location' : latlang}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var addressParts =  getAddressParts(results[0]);
                            // the city
                            var city = addressParts.locality;
                            // the state
                            var state = addressParts.administrative_area_level_1;
                            $("#ind_dis_state").val(state);
                            $("#ind_dis_city").val(city);
                            console.log('state name:'+state);
                            console.log('city name:'+city);
                        }
                    });
                }
            });
        });


        autocomplete1.addListener('place_changed', function () {
            // $('#long').val(place.geometry['location'].lng());
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById('distribution_world_api').value;
            console.log(address);
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    console.log(latitude, longitude);
                     $("#world_dis_lat").val(latitude);
                     $("#world_dis_lng").val(longitude);
                    const latlang = {
                        lat: parseFloat(latitude),
                        lng: parseFloat(longitude),
                    };
                    function getAddressParts(obj) {
                        var address = [];
                        obj.address_components.forEach( function(el) {
                            address[el.types[0]] = el.long_name;
                        });
                        return address;
                    } //getAddressParts()
                    geocoder.geocode( { 'location' : latlang}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var addressParts =  getAddressParts(results[0]);
                            // the city
                            var city = addressParts.locality;
                            // the state
                            var state = addressParts.administrative_area_level_1;
                            var country = addressParts.country;
                            $("#world_dis_city").val(city);
                            $("#world_dis_state").val(state);
                            $("#world_dis_country").val(country);
                            console.log('state name:'+state);
                            console.log('city name:'+city);
                            console.log('country:'+country);
                        }
                    });
                }
            });
        });


    }

</script>

<script >
    $(document).ready(function () {
        $("#datepicker").datepicker( {
            format: " yyyy", // Notice the Extra space at the beginning
            viewMode: "years",
            minViewMode: "years"
        });
        $(document).on('click','.btn1',function(){
            var taxonomy_scientific_name=$('#taxonomy_scientific_name').val();
            var taxonomy_accepted_name=$('#taxonomy_accepted_name').val();
            var taxonomy_reference=$('#taxonomy_reference').val();
            if(taxonomy_scientific_name ==='' || taxonomy_accepted_name ==='' || taxonomy_reference===''){

                swal("Scientific Name, Accepted Name and Reference Can't be Empty!", {
                    icon: "warning",

                });
                return false;

            }else{
                $.ajax({
                    url: '<?php echo base_url("crop/crop_pest_add/submit_form_one"); ?>',
                    type: 'post',
                    dataType: 'json',
                    data: $('form#myform').serialize(),
                    success: function(data) {

                        $.ajax({
                            url: '<?php echo base_url("crop/crop_pest_add/submit_form_two"); ?>',
                            type: 'post',
                            dataType: 'json',
                            data: $('form#myform2').serialize(),
                            success: function(data) {
                                $.ajax({
                                    url: '<?php echo base_url("crop/crop_pest_add/submit_form_three"); ?>',
                                    type: 'post',
                                    dataType: 'json',
                                    data: $('form#myform3').serialize(),
                                    success: function(data) {
                                        window.location.href = data.redirect;
                                    }
                                });
                            }
                        });



                    }
                });
            }


            //$('#toast-info').trigger('click');
        });

    });
</script>

<script >
    $(document).ready(function () {
        $("#datepicker").datepicker( {
            format: " yyyy", // Notice the Extra space at the beginning
            viewMode: "years",
            minViewMode: "years"
        });
        $(document).on('click','.btn1',function(){
            var taxonomy_scientific_name=$('#taxonomy_scientific_name').val();
            var taxonomy_accepted_name=$('#taxonomy_accepted_name').val();
            var taxonomy_reference=$('#taxonomy_reference').val();
            if(taxonomy_scientific_name ==='' || taxonomy_accepted_name ==='' || taxonomy_reference===''){

                swal("Scientific Name, Accepted Name and Reference Can't be Empty!", {
                    icon: "warning",

                });
                return false;

            }else{
                $.ajax({
                    url: '<?php echo base_url("crop/crop_pest_add/submit_form_one"); ?>',
                    type: 'post',
                    dataType: 'json',
                    data: $('form#myform').serialize(),
                    success: function(data) {

                        $.ajax({
                            url: '<?php echo base_url("crop/crop_pest_add/submit_form_two"); ?>',
                            type: 'post',
                            dataType: 'json',
                            data: $('form#myform2').serialize(),
                            success: function(data) {
                                $.ajax({
                                    url: '<?php echo base_url("crop/crop_pest_add/submit_form_three"); ?>',
                                    type: 'post',
                                    dataType: 'json',
                                    data: $('form#myform3').serialize(),
                                    success: function(data) {
                                        window.location.href = data.redirect;
                                    }
                                });
                            }
                        });



                    }
                });
            }


            //$('#toast-info').trigger('click');
        });

    });
</script>




<script>
    $("#confidence_one").rating();
    $("#confidence_two").rating();
    $("#confidence_three").rating();
    $(document).on('change','.confidence_one',function(){
        var prating = $(this).val();
        console.log('Number 1: '+ prating);
    });
    $(document).on('change','.confidence_two',function(){
        var prating = $(this).val();
        console.log('Number 2: '+ prating);

    });
    $(document).on('change','.confidence_three',function(){
        var prating = $(this).val();
        console.log('Number 3: '+ prating);

    });

</script>

<script>
    $("#s_sub_details").addClass('active');
</script>