

<div class="headerbar">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="headerbar-left">
        <ul class="header-nav header-nav-options">
            <li class="header-nav-brand" >
                <div class="brand-holder">
                    <?php
                        if($this->session->userdata('role')=='admin'){
                            echo' <a id="dashboard" href="'. base_url('admin/dashboard').'" >';
                        }
                    ?>
                        <span class="text-lg text-bold text-primary"> CROP PEST </span>
                    </a>
                </div>
            </li>
            <li>
                <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                  <i class="fa fa-bars"></i>
                </a>
            </li>
        </ul>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="headerbar-right">
        <ul class="header-nav header-nav-options">
            <li>
                <!-- Search form -->
                <form class="navbar-search" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" name="headerSearch" placeholder="Enter your keyword">
                    </div>
                    <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
                </form>
            </li>
            <li class="dropdown hidden-xs">
                <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                    <i class="fa fa-bell"></i><sup class="badge style-danger">4</sup>
                </a>
                <ul class="dropdown-menu animation-expand">
                    <li class="dropdown-header">Today's messages</li>
                    <li>
                        <a class="alert alert-callout alert-warning" href="javascript:void(0);">
                            <img class="pull-right img-circle dropdown-avatar"src="<?= base_url() ?>public/assets/img/avatar2.jpg?1404026449" alt="" />
                            <strong>Alex Anistor</strong><br/>
                            <small>Testing functionality...</small>
                        </a>
                    </li>
                    <li>
                        <a class="alert alert-callout alert-info" href="javascript:void(0);">
                            <img class="pull-right img-circle dropdown-avatarsrc="<?= base_url() ?>public/assets/img/avatar3.jpg?1404026799" alt="" />
                            <strong>Alicia Adell</strong><br/>
                            <small>Reviewing last changes...</small>
                        </a>
                    </li>
                    <li class="dropdown-header">Options</li>
                    <li><a href=" ">View all messages <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                    <li><a href=" ">Mark as read <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                </ul><!--end .dropdown-menu -->
            </li><!--end .dropdown -->
        </ul><!--end .header-nav-options -->
        <ul class="header-nav header-nav-profile">
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                    <?php
                    $this->db->select('*');
                    $this->db->from('ci_users');
                    $ses_email = $this->session->userdata('email');
                    $this->db->where('email',$ses_email);
                    $q= $this->db->get();
                    $result = $q->row_array();
                    ?>


                    <?php

                    $photo= $this->session->userdata('pic');
                    $name= $this->session->userdata('name');
                    $type= $this->session->userdata('role');

                    if($this->session->userdata('role')=='Patient'){
                    echo '<img src="'.base_url().'uploads/online_registration/'.$photo.'" />' ;
                    }else{
                    echo '<img src="'.base_url().'uploads/images/'.$result['profile_pic'].'" />' ;
                    }?>
                    <span class="profile-info">
                        <?php               if($this->session->userdata('role')=='Patient'){
                            echo $name;
                        }else{
                            echo $result['username'];
                        }?>
									<small><?= $type;  ?></small>
								</span>
                </a>
                <ul class="dropdown-menu animation-dock">
                    <!--<li class="dropdown-header">Config</li>-->
                    <li><?php
                        if($result['is_admin'] == 'admin') {
                            echo  '<a  href="'. base_url('admin/Users/userProfile/').$result['id'].'" >';

                        }
                        ?> My profile</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url('admin/auth/logout'); ?>"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                </ul><!--end .dropdown-menu -->
            </li><!--end .dropdown -->
        </ul><!--end .header-nav-profile -->
        <ul class="header-nav header-nav-toggle">
            <li>
            </li>
        </ul><!--end .header-nav-toggle -->
    </div><!--end #header-navbar-collapse -->
</div>