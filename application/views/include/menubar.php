<?php
    if($this->uri->segment(4)){
        $cur_tab = $this->uri->segment(2)==''?'': $this->uri->segment(2);
    }
    elseif($this->uri->segment(3) && $this->uri->segment(3) != 'cropDetails' ){
        $cur_tab = $this->uri->segment(3)==''?'': $this->uri->segment(3);
    }
    else{
        $cur_tab = $this->uri->segment(2)==''?'': $this->uri->segment(2);
    }


//$cur_tab = $this->uri->segment(2)==''?'': $this->uri->segment(2);
//$cur_tab2 = $this->uri->segment(3)==''?'': $this->uri->segment(3);
?>
<div class="menubar-fixed-panel">
    <div>
        <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </div>
    <div class="expanded">
        <a href="">
            <span class="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
        </a>
    </div>
</div>
<div class="menubar-scroll-panel">
    <!-- BEGIN MAIN MENU -->
    <ul id="main-menu" class="gui-controls">

        <!-- BEGIN DASHBOARD -->
        <?php
        //if($this->session->userdata('role')=='admin'){
        if($this->session->userdata('role')=='admin'){
            echo '<li>
            <a id="Dashboard" href="'. base_url('admin/Dashboard').'" >
                <div class="gui-icon"><i class="md md-home"></i></div>
                <span class="title">Dashboard</span>
            </a>
        </li>
        ';
        }
        ?><!--end /menu-li -->
        <!-- END DASHBOARD -->
        <?php
        if($this->session->userdata('add_crop_pest') == 1 || $this->session->userdata('view_crop_pest') == 1 ){ ?>
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="fa fa-money"></i></div>
                    <span class="title">&ensp;Crop Pest</span>
                </a>
                <!--start submenu -->
                <ul>
                    <?php if($this->session->userdata('add_crop_pest') == 1)  {
                        echo '<li><a href="'. base_url('crop/Crop_pest_add').'" id="Crop_pest_add"><span class="title">Add Crop Pest</span></a></li>';
                    } if($this->session->userdata('view_crop_pest') == 1)  {
                        echo '<li><a href="'.base_url('crop/Crop_pest_view').'" id="Crop_pest_view"><span class="title">View Crop Pest</span></a></li>';
                    } ?>
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->

        <?php } ?>
        <?php
        if($this->session->userdata('view_users') == 1 || $this->session->userdata('add_users') == 1 || $this->session->userdata('add_roles') == 1 || $this->session->userdata('add_permission') == 1 ){ ?>
            <li class="gui-folder">
            <a>
                <div class="gui-icon"><i class="fa fa-users"></i></div>
                <span class="title">&ensp;Manage Users </span>
            </a>
            <!--start submenu -->
            <ul>
            <?php if($this->session->userdata('view_users') == 1)
            {
                echo '<li><a href="'. base_url('admin/Users').'" id="Users"><span class="title">View Users</span></a></li>';
            } if($this->session->userdata('add_users') == 1)
            {
                echo '<li><a href="'. base_url('admin/Users/add').'" id="add"><span class="title">Add Users</span></a></li>';
            } if($this->session->userdata('add_roles') == 1)
            {
                echo '<li><a href="'.base_url('admin/Users/role_list').'" id="role_list"><span class="title">Add Roles</span></a></li>';
            } if($this->session->userdata('add_permission') == 1)
            {
                echo '<li><a href="'.base_url('admin/Users/set_permission').'" id="set_permission"><span class="title">Add Permissions</span></a></li>';
            }
                ?>
                </ul><!--end /submenu -->
                </li><!--end /menu-li -->

            <?php } ?>

    </ul><!--end .main-menu -->
    <!-- END MAIN MENU -->
    <div class="menubar-foot-panel">
        <small class="no-linebreak hidden-folded">
            <span class="opacity-75">Copyright &copy; 2020</span> <strong>Yakairol Engineering <br> Pvt. Ltd.</strong> All rights
            reserved.
        </small>
    </div>
</div>

<script>
    $("#<?= $cur_tab; ?>").addClass('active');
</script>
