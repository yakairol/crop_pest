<!DOCTYPE html>
<html lang="en">
<head>
    <title>CROP PEST</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->

    <!-------- common for every page ----------->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/><!-- for fontsize stylling -->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/bootstrap.css" />
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/materialadmin.css?1425466319" /> <!-- css for material admin-->

    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/font-awesome.min.css?1422529194" /> <!-- side bar icon-->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/material-design-iconic-font.min.css?1421434286" /><!-- side bar icon-->

    <!----- For datatable ---------->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989" />
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990" />
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990" /><!-- for printing -->

    <!--- For Select ----->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/select2/select2.css?1424887856" /><!--for select -->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/multi-select/multi-select.css?1424887857" /><!--for multi select -->

    <!-- For Datepicker --->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858" /><!-- for datepicker -->

    <!---- my custom css -->
    <link rel="stylesheet" href="<?= base_url() ?>public/assets/css/custom.css" /> <!-- custom css -->

    <!--- for printing ------>
    <link type="text/css" rel="stylesheet" href="<?= base_url() ?>public/assets/css/theme-default/materialadmin_print.css?1419847669"  media="print"/>

    <!-- END STYLESHEETS -->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
















</head>
<!--<body class="menubar-hoverable header-fixed">-->
<?php
    if($this->uri->segment(2) == 'Dashboard' || $this->uri->segment(2) == 'Crop_pest_add'){ ?>
        <body class="menubar-hoverable header-fixed">
   <?php }
   else{ ?>
   <body class="menubar-hoverable header-fixed menubar-pin">
   <?php }
?>
<!--<body class="menubar-hoverable header-fixed menubar-pin">-->
    <header id="header" >
        <?php include('include/header.php'); ?>
    </header>

    <div id="base">
        <!-- BEGIN OFFCANVAS LEFT -->
        <!-- END OFFCANVAS LEFT -->
        <div id="content">
            <section>
                <!--<div class="section-body">-->
                    <?php// include('include/content.php'); ?>
                    <?php $this->load->view($view);?>
               <!-- </div>-->
            </section>
        </div>
        <div id="menubar" class="menubar-inverse animate">
            <?php include('include/menubar.php'); ?>
        </div>

    </div>

    <!-- BEGIN JAVASCRIPT -->

    <!-------- common for every page ----------->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/bootstrap/bootstrap.min.js"></script> <!-- for header navigation -->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/App.js"></script><!-- menubar data toogle minimize maximize-->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/AppNavigation.js"></script><!-- menubar data toogle minimize maximize-->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/AppOffcanvas.js"></script> <!-- right side comvas -->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/AppNavSearch.js"></script><!-- for navigation bar search -->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script> <!-- for sidebar dropdown -->

    <!-- for datatable page -------------------->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/DataTables/jquery.dataTables.min.js"></script><!-- for datable-->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script><!-- for datatable colvis-->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script><!-- for datatable print-->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/demo/DemoTableDynamic.js"></script>

    <!------- For select --------------->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/select2/select2.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/demo/DemoFormComponents.js"></script>

    <!-- for date picker ------>
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/demo/DemoFormComponents.js"></script>

    <!---- for form with select ----------->
    <script type="text/javascript" src="<?= base_url() ?>public/assets/js/core/source/AppForm.js"></script>

    <!-- for form validation -->

    <!-- for data tongle tooltip---->
    <!--<script src="<?//= base_url() ?>public/assets/js/libs/jquery/jquery-1.11.2.min.js"></script>-->
    <script src="<?= base_url() ?>public/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>

    <!------  for tabs navigation ---->
    <script src="<?= base_url() ?>public/assets/js/core/source/AppVendor.js"></script>

   <!--------- for crop pest -------------->
   <script src="<?= base_url() ?>public/assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>

   <script src="<?= base_url() ?>public/assets/js/core/demo/DemoFormWizard.js"></script>








   <!-- END JAVASCRIPT -->
</body>
</html>