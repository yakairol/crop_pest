-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 09, 2020 at 11:43 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `croppest_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_info`
--

DROP TABLE IF EXISTS `add_info`;
CREATE TABLE IF NOT EXISTS `add_info` (
  `add_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_info_name` varchar(200) DEFAULT NULL,
  `add_info_ref` varchar(200) DEFAULT NULL,
  `add_info_uni_id` varchar(200) DEFAULT NULL,
  `add_info_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`add_info_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

DROP TABLE IF EXISTS `ci_users`;
CREATE TABLE IF NOT EXISTS `ci_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` varchar(20) DEFAULT NULL,
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `is_admin`, `last_ip`, `created_at`, `updated_at`, `status`, `profile_pic`) VALUES
(1, 'world Admin', 'world', 'Admin', 'admin@admin.com', '9862762423', '$2y$10$OdwJfinutrCFdlcf8YmKU.zp.04SJLMaiCdiywf.aqmTIclthv8Em', 'admin', '', '2019-04-04 10:32:59', '2019-07-23 05:07:12', 1, 'm-blog-4.jpg'),
(11, 'Doc John', 'Doc', 'John', 'doc@mail.com', '9856181675', '$2y$10$iMEUUBKDfmxQzLD3ivtb9utMSTi3OY/68vcj8ynVPen2CsgALOElq', 'doctor', '', '2019-04-13 07:04:56', '2019-04-13 07:04:56', 0, ''),
(13, 'Jexina Devi', 'Jexina', 'Devi', 'jex@doc.com', '9856181675', '$2y$10$O4or4dAQWLEkeXo.NJWMR.DqVOgVmgEJSxFtfwHyj9kcy.R6IMWiO', 'doctor', '', '2019-06-10 06:06:01', '2020-09-01 11:59:29', 0, '1560147480485510bd5a-1f8d-4486-b26e-6880b3f98cea.jpg'),
(3, 'bijoy oijam', 'bijoy', 'oijam', 'bijoy@bijoy.com', '9856181675', '$2y$10$l9qswljF7PN6PBO.6ZJ.vuz1WBC/UnVvRtGXC.EYX7EUu4LoftiLe', 'doctor', '', '2020-08-22 09:08:52', '2020-08-22 09:08:52', 0, '159809009230IMG-20181227-WA0003.jpg'),
(24, 'doc4 doc4', 'doc4', 'doc4', 'doc4@doc4.com', '4444444444', '$2y$10$UOgvLV9Wrwt92Ph0k0NqIObJJFdt.TfjVqAOwntPlATWovGu5EjYu', 'doctor', '', '2020-09-19 09:09:40', '2020-09-19 09:09:40', 0, '160050843976dfdsfww.JPG'),
(23, 'doc3 doc3', 'doc3', 'doc3', 'doc3@doc3.com', '3333333333', '$2y$10$Be07X4bt0bXl87P8S//tV.kOWscTCroYZVEgnvF2FT014q9WjpVI.', 'doctor', '', '2020-09-19 09:09:55', '2020-09-19 09:09:55', 0, '160050815484erererererer.JPG'),
(18, 'doctor4 singh', 'doctor4', 'singh', 'doctor4@doctor4.com', '1122334455', '$2y$10$dtaKUtdq/ypvifgkWsqGi.k0BbT98K1ZQdXIpU5XwkPFuvWPNEjuK', 'doctor', '', '2020-09-19 09:09:25', '2020-09-19 09:09:25', 0, '160050632493qq.JPG'),
(22, 'doc2 doc2', 'doc2', 'doc2', 'doc2@doc2.com', '2222222222', '$2y$10$LZROPTG.op0.IHJ92.y79ef5wNnzSK0RwEhW/bNDRVE4EnPUxxlSq', 'doctor', '', '2020-09-19 09:09:39', '2020-09-19 09:09:39', 0, '160050807955sssfjjkk.JPG'),
(21, 'doc1 doc1', 'doc1', 'doc1', 'doc1@doc1.com', '1111111111', '$2y$10$JuDmE3EfkkYYfxyHZIKGFewv97U9m8VY/VHoymFnczP6KtsvpqwOO', 'doctor', '', '2020-09-19 09:09:42', '2020-09-19 09:09:42', 0, '160050796198werwerwerwerwer.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `crop_pest_details`
--

DROP TABLE IF EXISTS `crop_pest_details`;
CREATE TABLE IF NOT EXISTS `crop_pest_details` (
  `pest_det_id` int(11) NOT NULL AUTO_INCREMENT,
  `pest_det_crop` varchar(200) DEFAULT NULL,
  `pest_det_pest` varchar(200) DEFAULT NULL,
  `pest_det_uni_id` varchar(200) DEFAULT NULL,
  `pest_det_uploader` varchar(200) DEFAULT NULL,
  `pest_det_category` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`pest_det_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crop_pest_details`
--

INSERT INTO `crop_pest_details` (`pest_det_id`, `pest_det_crop`, `pest_det_pest`, `pest_det_uni_id`, `pest_det_uploader`, `pest_det_category`) VALUES
(1, 'RICE\r\n', 'Acanthus ilicifolus\r\n', '1', '2', 'CEREAL');

-- --------------------------------------------------------

--
-- Table structure for table `distribution`
--

DROP TABLE IF EXISTS `distribution`;
CREATE TABLE IF NOT EXISTS `distribution` (
  `distribution_id` int(11) NOT NULL AUTO_INCREMENT,
  `distribution_world_api` varchar(200) DEFAULT NULL,
  `distribution_india_api` varchar(200) DEFAULT NULL,
  `distribution_district` varchar(200) DEFAULT NULL,
  `distribution_uni_id` varchar(200) DEFAULT NULL,
  `distribution_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`distribution_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distribution`
--

INSERT INTO `distribution` (`distribution_id`, `distribution_world_api`, `distribution_india_api`, `distribution_district`, `distribution_uni_id`, `distribution_uploader`) VALUES
(1, 'WORLD DISTRIBUTION-API', 'INDIA DISTRIBUTION (STATES)- API', 'DISTRICTS\r\n', '1', '2');

-- --------------------------------------------------------

--
-- Table structure for table `general_images`
--

DROP TABLE IF EXISTS `general_images`;
CREATE TABLE IF NOT EXISTS `general_images` (
  `general_images_id` int(11) NOT NULL AUTO_INCREMENT,
  `general_images_name` varchar(200) DEFAULT NULL,
  `general_images_uni_id` varchar(200) DEFAULT NULL,
  `general_images_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`general_images_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ife_cycle_images`
--

DROP TABLE IF EXISTS `ife_cycle_images`;
CREATE TABLE IF NOT EXISTS `ife_cycle_images` (
  `ife_cycle_images_id` int(11) NOT NULL AUTO_INCREMENT,
  `ife_cycle_images_name` varchar(200) DEFAULT NULL,
  `ife_cycle_images_ref` varchar(200) DEFAULT NULL,
  `ife_cycle_images_uni_id` varchar(200) DEFAULT NULL,
  `ife_cycle_images_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ife_cycle_images_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `life_cycle`
--

DROP TABLE IF EXISTS `life_cycle`;
CREATE TABLE IF NOT EXISTS `life_cycle` (
  `life_cycle_id` int(11) NOT NULL AUTO_INCREMENT,
  `life_cycle_field_condition` varchar(200) DEFAULT NULL,
  `life_cycle_field_condition_ref` varchar(200) DEFAULT NULL,
  `life_cycle_oviposition_site` varchar(200) DEFAULT NULL,
  `life_cycle_oviposition_site_ref` varchar(200) DEFAULT NULL,
  `life_cycle_diapause` varchar(200) DEFAULT NULL,
  `life_cycle_diapause_ref` varchar(200) DEFAULT NULL,
  `life_cycle_pupation_site` varchar(200) DEFAULT NULL,
  `life_cycle_pupation_site_ref` varchar(200) DEFAULT NULL,
  `life_cycle_lab_condition` varchar(200) DEFAULT NULL,
  `life_cycle_lab_condition_ref` varchar(200) DEFAULT NULL,
  `life_cycle_bio_ecology` varchar(200) DEFAULT NULL,
  `life_cycle_bio_ecology_ref` varchar(200) DEFAULT NULL,
  `life_cycle_dispersal` varchar(200) DEFAULT NULL,
  `life_cycle_dispersal_ref` varchar(200) DEFAULT NULL,
  `life_cycle_uni_id` varchar(200) DEFAULT NULL,
  `life_cycle_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`life_cycle_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `natural_history`
--

DROP TABLE IF EXISTS `natural_history`;
CREATE TABLE IF NOT EXISTS `natural_history` (
  `natural_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `natural_history_intro` varchar(200) DEFAULT NULL,
  `natural_history_intro_ref` varchar(200) DEFAULT NULL,
  `natural_history_stage` varchar(200) DEFAULT NULL,
  `natural_history_stage_ref` varchar(200) DEFAULT NULL,
  `natural_history_affected_part` varchar(200) DEFAULT NULL,
  `natural_history_affected_part_ref` varchar(200) DEFAULT NULL,
  `natural_history_damage_symtoms` varchar(200) DEFAULT NULL,
  `natural_history_position_site` varchar(200) DEFAULT NULL,
  `natural_history_position_site_ref` varchar(200) DEFAULT NULL,
  `natural_history_pupation_site` varchar(200) DEFAULT NULL,
  `natural_history_pupation_site_ref` varchar(200) DEFAULT NULL,
  `natural_history_diapause` varchar(200) DEFAULT NULL,
  `natural_history_diapause_ref` varbinary(200) DEFAULT NULL,
  `natural_history_uni_id` varchar(200) DEFAULT NULL,
  `natural_history_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`natural_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `online_registration`
--

DROP TABLE IF EXISTS `online_registration`;
CREATE TABLE IF NOT EXISTS `online_registration` (
  `online_registration_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(200) DEFAULT NULL,
  `spouse_name` varchar(200) DEFAULT NULL,
  `dob` varchar(200) DEFAULT NULL,
  `blood_grp` varchar(200) DEFAULT NULL,
  `height` varchar(200) DEFAULT NULL,
  `weight` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `contact` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `emer_contact` varchar(200) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `gendre` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`online_registration_id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `online_registration`
--

INSERT INTO `online_registration` (`online_registration_id`, `full_name`, `spouse_name`, `dob`, `blood_grp`, `height`, `weight`, `status`, `contact`, `password`, `location`, `address`, `emer_contact`, `photo`, `gendre`, `type`) VALUES
(18, 'Dhananjoy Singh', 'Chaoba', '01/03/1994', 'O -ve', '123', '78', 'Married', '9856181675', '$2y$10$O4or4dAQWLEkeXo.NJWMR.DqVOgVmgEJSxFtfwHyj9kcy.R6IMWiO', 'Manipur', 'Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09', '9856181675', 'admin.png', 'Male', 'Patient'),
(35, 'sanamatum', 'somi', '08/12/2010', 'A +ve', '45', '44', 'Married', '9856555555', '$2y$10$4.75qdV80otS2vqBdgwNguZ/qJMo3RHCzQJeU2uPVeN5agVE1venm', 'ukhrul', 'ukhrul district manipur india', '5555555555', 'ggkkppss.JPG', 'male', 'Patient'),
(31, 'tomba', 'tombi', '09/16/2020', 'A -ve', '23', '56', 'Married', '9856111111', '$2y$10$UZz8x44Sg9.N9.pXn37IE.Bo5tiMLtsavZnuSMqITCd.LC3iqW1h2', 'thoubal', 'manipur imphal india ', '1111111111', 'sdfsdfswswwwll.JPG', 'male', 'Patient'),
(32, 'Rakesh ', 'bobby', '09/04/2020', 'A +ve', '67', '44', 'Married', '9856222222', '$2y$10$uKb2C1O13RnjOjb2hZ39q.JjFouImYOb7pghSFaw4E3S2iWDCvpc6', 'manipur', 'thoubal manipur imphal india', '2222222222', 'wewwklklhh.JPG', 'male', 'Patient'),
(33, 'Naobi ', 'baba', '09/02/2020', 'A +ve', '54', '66', 'Single', '9856333333', '$2y$10$wGIU3pscnChyvbD34HXtHOVxBTX8WAFQAj4KzK2BSX.t8zoGNW9Aa', 'kakching', 'kakching thoual imphal maipur', '3333333333', 'nnhhffjuu.JPG', 'female', 'Patient'),
(34, 'tomthin', 'kulubi', '09/01/2020', 'B +ve', '44', '45', 'Married', '9856444444', '$2y$10$8COXzw.r18syki/DpMX6jewf3hxKEwSXaI/1miEP9O3vGzvRAoxsO', 'senapati', 'senapati district manipur india ', '4444444444', 'ggttff.JPG', 'male', 'Patient');

-- --------------------------------------------------------

--
-- Table structure for table `paper_reference`
--

DROP TABLE IF EXISTS `paper_reference`;
CREATE TABLE IF NOT EXISTS `paper_reference` (
  `paper_reference_id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_reference_category` varchar(200) DEFAULT NULL,
  `paper_reference_crop` varchar(200) DEFAULT NULL,
  `paper_reference_pest` varchar(200) DEFAULT NULL,
  `paper_reference_pest_status` varchar(200) DEFAULT NULL,
  `paper_reference_uni_id` varchar(200) DEFAULT NULL,
  `paper_reference_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`paper_reference_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paper_reference`
--

INSERT INTO `paper_reference` (`paper_reference_id`, `paper_reference_category`, `paper_reference_crop`, `paper_reference_pest`, `paper_reference_pest_status`, `paper_reference_uni_id`, `paper_reference_uploader`) VALUES
(1, 'CEREAL', 'RICE', 'Acanthus ilicifolus', 'MAJOR', '1', '2');

-- --------------------------------------------------------

--
-- Table structure for table `parasite`
--

DROP TABLE IF EXISTS `parasite`;
CREATE TABLE IF NOT EXISTS `parasite` (
  `parasite_id` int(11) NOT NULL AUTO_INCREMENT,
  `parasite_name` varchar(200) DEFAULT NULL,
  `parasite_stage` varchar(200) DEFAULT NULL,
  `parasite_ref` varchar(200) DEFAULT NULL,
  `parasite_uni_id` varchar(200) DEFAULT NULL,
  `parasite_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`parasite_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pradators`
--

DROP TABLE IF EXISTS `pradators`;
CREATE TABLE IF NOT EXISTS `pradators` (
  `pradators_id` int(11) NOT NULL AUTO_INCREMENT,
  `pradators_name` varchar(200) DEFAULT NULL,
  `pradators_stage` varchar(200) DEFAULT NULL,
  `pradators_ref` varchar(200) DEFAULT NULL,
  `pradators_uni_id` varchar(200) DEFAULT NULL,
  `pradators_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`pradators_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prevention`
--

DROP TABLE IF EXISTS `prevention`;
CREATE TABLE IF NOT EXISTS `prevention` (
  `prevention_id` int(11) NOT NULL AUTO_INCREMENT,
  `prevention_tel` varchar(200) DEFAULT NULL,
  `prevention_etl_ref` varchar(200) DEFAULT NULL,
  `prevention_cultural` varchar(200) DEFAULT NULL,
  `prevention_cultural_ref` varchar(200) DEFAULT NULL,
  `prevention_mechanical` varchar(200) DEFAULT NULL,
  `prevention_mechanical_ref` varchar(200) DEFAULT NULL,
  `prevention_physical` varchar(200) DEFAULT NULL,
  `prevention_physical_ref` varchar(200) DEFAULT NULL,
  `prevention_biological` varchar(200) DEFAULT NULL,
  `prevention_biological_ref` varchar(200) DEFAULT NULL,
  `prevention_insecticide` varchar(200) DEFAULT NULL,
  `prevention_insecticide_ref` varchar(200) DEFAULT NULL,
  `prevention_trade_name` varchar(200) DEFAULT NULL,
  `prevention_company` varchar(200) DEFAULT NULL,
  `prevention_active_ingredient` varchar(200) DEFAULT NULL,
  `prevention_dosage` varchar(200) DEFAULT NULL,
  `prevention_resistant_variety` varchar(200) DEFAULT NULL,
  `prevention_resistant_variety_ref` varchar(200) DEFAULT NULL,
  `prevention_tolerant_name` varchar(200) DEFAULT NULL,
  `prevention_tolerant_name_ref` varchar(200) DEFAULT NULL,
  `prevention_uni_id` varchar(200) DEFAULT NULL,
  `prevention_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`prevention_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_type`
--

DROP TABLE IF EXISTS `role_type`;
CREATE TABLE IF NOT EXISTS `role_type` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_type_name` varchar(100) DEFAULT NULL,
  `bed_category` int(1) DEFAULT NULL,
  `bed_details` int(1) DEFAULT NULL,
  `bed_allotment` int(1) DEFAULT NULL,
  `add_appointment` int(1) DEFAULT NULL,
  `todays_appointment` int(1) DEFAULT NULL,
  `patient_list` int(1) DEFAULT NULL,
  `online_add_appoint` int(1) DEFAULT NULL,
  `online_patient` int(1) DEFAULT NULL,
  `my_prescription` int(1) DEFAULT NULL,
  `doctor_schedule` int(1) DEFAULT NULL,
  `lab_category` int(1) DEFAULT NULL,
  `lab_profile` int(1) DEFAULT NULL,
  `lab_test` int(1) DEFAULT NULL,
  `lab_patient_list` int(1) DEFAULT NULL,
  `add_med_type` int(1) DEFAULT NULL,
  `add_medicine` int(1) DEFAULT NULL,
  `add_vaccines` int(1) DEFAULT NULL,
  `immu_patient` int(1) DEFAULT NULL,
  `manage_driver` int(1) DEFAULT NULL,
  `manage_ambulance` int(1) DEFAULT NULL,
  `assign_ambulance` int(1) DEFAULT NULL,
  `add_billing` int(1) DEFAULT NULL,
  `manage_billing` int(1) DEFAULT NULL,
  `manage_department` int(1) DEFAULT NULL,
  `setting` int(1) DEFAULT NULL,
  `clinic_setting` int(1) DEFAULT NULL,
  `edit_home_page` int(1) DEFAULT NULL,
  `sms_setting` int(1) DEFAULT NULL,
  `view_users` int(1) DEFAULT NULL,
  `add_users` int(1) DEFAULT NULL,
  `add_roles` int(1) DEFAULT NULL,
  `add_permission` int(1) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_type`
--

INSERT INTO `role_type` (`role_id`, `role_type_name`, `bed_category`, `bed_details`, `bed_allotment`, `add_appointment`, `todays_appointment`, `patient_list`, `online_add_appoint`, `online_patient`, `my_prescription`, `doctor_schedule`, `lab_category`, `lab_profile`, `lab_test`, `lab_patient_list`, `add_med_type`, `add_medicine`, `add_vaccines`, `immu_patient`, `manage_driver`, `manage_ambulance`, `assign_ambulance`, `add_billing`, `manage_billing`, `manage_department`, `setting`, `clinic_setting`, `edit_home_page`, `sms_setting`, `view_users`, `add_users`, `add_roles`, `add_permission`) VALUES
(6, 'admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(7, 'doctor', 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 'nurse', 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 'staff', 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(22, 'security', 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(24, 'lab assistant', 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(23, 'opd staff', 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `set_permission`
--

DROP TABLE IF EXISTS `set_permission`;
CREATE TABLE IF NOT EXISTS `set_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(100) DEFAULT NULL,
  `all_member` int(1) DEFAULT NULL,
  `doctor` int(1) DEFAULT NULL,
  `nurse` int(1) DEFAULT NULL,
  `staff` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `set_permission`
--

INSERT INTO `set_permission` (`id`, `link`, `all_member`, `doctor`, `nurse`, `staff`) VALUES
(1, 'Page 1', 1, 0, 1, 1),
(3, 'Page 2', 0, 1, 1, 0),
(16, 'admission', 0, 1, 0, 1),
(13, 'Page 3', 1, 1, 1, 0),
(14, 'Page 4', 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `symtom_images`
--

DROP TABLE IF EXISTS `symtom_images`;
CREATE TABLE IF NOT EXISTS `symtom_images` (
  `symtom_images_id` int(11) NOT NULL AUTO_INCREMENT,
  `symtom_images_name` varchar(200) DEFAULT NULL,
  `symtom_images_uni_id` varchar(200) DEFAULT NULL,
  `symtom_images_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`symtom_images_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `taxonomy`
--

DROP TABLE IF EXISTS `taxonomy`;
CREATE TABLE IF NOT EXISTS `taxonomy` (
  `taxonomy_id` int(11) NOT NULL AUTO_INCREMENT,
  `taxonomy_kingdom` varchar(200) DEFAULT NULL,
  `taxonomy_phylum` varchar(200) DEFAULT NULL,
  `taxonomy_class` varchar(200) DEFAULT NULL,
  `taxonomy_order` varchar(200) DEFAULT NULL,
  `taxonomy_family` varchar(200) DEFAULT NULL,
  `taxonomy_genus` varchar(200) DEFAULT NULL,
  `taxonomy_species` varchar(200) DEFAULT NULL,
  `taxonomy_sub_species` varchar(200) DEFAULT NULL,
  `taxonomy_variety` varchar(200) DEFAULT NULL,
  `taxonomy_accepted_name` varchar(200) DEFAULT NULL,
  `taxonomy_scientific_name` varchar(200) DEFAULT NULL,
  `taxonomy_reference` varchar(200) DEFAULT NULL,
  `taxonomy_published_year` varchar(200) DEFAULT NULL,
  `taxonomy_published_in` varchar(200) DEFAULT NULL,
  `taxonomy_nomen_code` varchar(200) DEFAULT NULL,
  `taxonomy_published` varchar(200) DEFAULT NULL,
  `taxonomy_uni_id` varchar(200) DEFAULT NULL,
  `taxonomy_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`taxonomy_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taxonomy`
--

INSERT INTO `taxonomy` (`taxonomy_id`, `taxonomy_kingdom`, `taxonomy_phylum`, `taxonomy_class`, `taxonomy_order`, `taxonomy_family`, `taxonomy_genus`, `taxonomy_species`, `taxonomy_sub_species`, `taxonomy_variety`, `taxonomy_accepted_name`, `taxonomy_scientific_name`, `taxonomy_reference`, `taxonomy_published_year`, `taxonomy_published_in`, `taxonomy_nomen_code`, `taxonomy_published`, `taxonomy_uni_id`, `taxonomy_uploader`) VALUES
(1, 'Plantae', 'Angiosperm', 'Eudicots', 'Lamiales', 'Acanthaceae', 'Acanthus', 'Acanthus ilifolius', 'Acanthus ilifolius. indica', 'Acanthus ilifolius. indica', 'Acanthus ilicifolus (L.)', 'Acanthus ilicifolus\r\n', 'Sp. Pl. 2: 639', '1753', 'Species Plantarum Vol.2', 'Lectotype: Herb. Linn. No 816.6', 'Journal of Adelaide Bot. Grad, 9:68-1986', '1', '2');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomy_name`
--

DROP TABLE IF EXISTS `taxonomy_name`;
CREATE TABLE IF NOT EXISTS `taxonomy_name` (
  `taxonomy_name_id` int(11) NOT NULL AUTO_INCREMENT,
  `common_ver_name` varchar(200) DEFAULT NULL,
  `taxonomy_vernacular_name` varchar(200) DEFAULT NULL,
  `taxonomy_name_uni_id` varchar(200) DEFAULT NULL,
  `taxonomy_name_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`taxonomy_name_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taxonomy_name`
--

INSERT INTO `taxonomy_name` (`taxonomy_name_id`, `common_ver_name`, `taxonomy_vernacular_name`, `taxonomy_name_uni_id`, `taxonomy_name_uploader`) VALUES
(1, 'text 1', 'text 2', '1', '2'),
(2, 'text 3', 'text 4', '1', '2');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomy_syn`
--

DROP TABLE IF EXISTS `taxonomy_syn`;
CREATE TABLE IF NOT EXISTS `taxonomy_syn` (
  `taxonomy_syn_id` int(11) NOT NULL AUTO_INCREMENT,
  `taxonomy_syn_name` varchar(200) DEFAULT NULL,
  `taxonomy_syn_ref` varchar(200) DEFAULT NULL,
  `taxonomy_syn_year` varchar(200) DEFAULT NULL,
  `taxonomy_syn_uni_id` varchar(200) DEFAULT NULL,
  `taxonomy_syn_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`taxonomy_syn_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taxonomy_syn`
--

INSERT INTO `taxonomy_syn` (`taxonomy_syn_id`, `taxonomy_syn_name`, `taxonomy_syn_ref`, `taxonomy_syn_year`, `taxonomy_syn_uni_id`, `taxonomy_syn_uploader`) VALUES
(1, 'Acanthus ilicifolus (L).Juss', 'Gen. Pl. (Jussi).103\r\n', '1789\r\n', '1', '2'),
(2, 'Acanthus ilicifolus (L).Juss', 'Gen. Pl. (Jussi).103\r\n', '1789\r\n', '1', '2');

-- --------------------------------------------------------

--
-- Table structure for table `yield_loss`
--

DROP TABLE IF EXISTS `yield_loss`;
CREATE TABLE IF NOT EXISTS `yield_loss` (
  `yield_loss_id` int(11) NOT NULL AUTO_INCREMENT,
  `yield_loss_etl` varchar(200) DEFAULT NULL,
  `yield_loss_name` varchar(200) DEFAULT NULL,
  `yield_loss_ref` varchar(200) DEFAULT NULL,
  `yield_loss_uni_id` varchar(200) DEFAULT NULL,
  `yield_loss_uploader` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`yield_loss_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
