/**
 * French translation for bootstrap-datetimepicker
 * Nico Mollet <nico.mollet@gmail.com>
 */
;(function($){
	$.fn.datetimepicker.dates['fr'] = {
		days: ["Nongmaijing", "Ningthoukaba", "Leibakpokpa", "Yumshakeisha", "Sagoolsen", "Eerai", "Thangja", "Nongmaijing"],
		daysShort: ["Nong", "Ning", "Lei", "Yum", "Sagol", "Eerai", "Thang", "Nong"],
		daysMin: ["No", "Ni", "Le", "Yu", "Sa", "Er", "Th", "No"],
		months: ["January", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
		monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
		today: "Aujourd'hui",
		suffix: [],
		meridiem: ["am", "pm"],
		weekStart: 1,
		format: "dd/mm/yyyy hh:ii"
	};
}(jQuery));
